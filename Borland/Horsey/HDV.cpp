//---------------------------------------------------------------------------
#include <vcl.h>
#include <inifiles.hpp>

#pragma hdrstop

#include "HDV.h"
#include "horsey2.h"
#include "RulingFigures.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFDayValue *FDayValue;


//---------------------------------------------------------------------------
__fastcall TFDayValue::TFDayValue(TComponent* Owner)
        : TForm(Owner)
{
        TIniFile *ini;
        
        ini = new TIniFile(ChangeFileExt(Application->ExeName,".INI"));
        Top = ini->ReadInteger("DayForm","Top",10);
        Left = ini->ReadInteger("DayForm","Left",10);

        DecodeDate(Now(),wYear, wMonth, wDay);

        delete ini;
}
//---------------------------------------------------------------------------
void __fastcall TFDayValue::FormCreate(TObject *Sender)
{
        MonthCalendar1->Date = EncodeDate(wYear, wMonth, wDay);
        Caption="Day Value";
        EDValueRefresh(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TFDayValue::EDValueRefresh(TObject *Sender)
{
        DecodeDate(MonthCalendar1->Date, wYear, wMonth, wDay);
        EDValue->Text=IntToStr(GetDaysValue());
}

//---------------------------------------------------------------------------

void __fastcall TFDayValue::Button1Click(TObject *Sender)
{
        TIniFile *ini;

        ini = new TIniFile(ChangeFileExt(Application->ExeName,".INI"));
        ini->WriteInteger("DayForm","Top",Top);
        ini->WriteInteger("DayForm","Left",Left);
        DecodeDate(MonthCalendar1->Date, wYear, wMonth, wDay);
        FDayValue->Hide();
        delete ini;
}
//---------------------------------------------------------------------------
void __fastcall TFDayValue::SetDateStruct(TObject *Sender)
{
        DecodeDate(MonthCalendar1->Date, wYear, wMonth, wDay);
        EDValueRefresh(Sender);
}

//-----------------------------------------------------------------------------
// Get the Date string in dd/mm/yyyy format 
AnsiString TFDayValue::GetDateStr(void)
{
        return(DateToStr(MonthCalendar1->Date));
}

//----------------------------------------------------------------------------
// returns the days value
int TFDayValue::GetDaysValue(void)
{
        RF rf;

        return(rf.DaysValue(wDay,wMonth));
}

//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>
//extract the race date from a string
int TFDayValue::ExDate(char *s)
{
        char Buff[256];
        int x;

        x=0;
        while(*s != '.')
        {
                Buff[x]=*s;
                s++;
                x++;
        }
        Buff[x]=0;
        return(atoi(Buff));
}


//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>
// extract the month from the string
//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
int TFDayValue::ExMonth(char *s)
{
        char Buff[256];
        int x;

        x=0;
        while(*s != '.') s++;
        s++;
        while(*s != '.')
        {
                Buff[x]=*s;
                s++;
                x++;
        }
        Buff[x]=0;
        return(atoi(Buff));
}

//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>
// Extract the year from string
//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>
int TFDayValue::ExYear(char *s)
{
        char Buff[256];
        int x;

        x=0;
        while(*s != '.') s++;
        s++;
        while(*s != '.') s++;
        s++;
        while(*s != ',')
        {
                Buff[x]=*s;
                s++;
                x++;
        }
        Buff[x]=0;
        return(atoi(Buff));
}
void __fastcall TFDayValue::FormClose(TObject *Sender,
      TCloseAction &Action)
{
        TIniFile *ini;

        ini = new TIniFile(ChangeFileExt(Application->ExeName,".INI"));
        ini->WriteInteger("DayForm","Top",Top);
        ini->WriteInteger("DayForm","Left",Left);

        delete ini;
}
//---------------------------------------------------------------------------

