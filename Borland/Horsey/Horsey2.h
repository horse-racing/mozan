//---------------------------------------------------------------------------
#ifndef Horsey2H
#define Horsey2H
//---------------------------------------------------------------------------

#define MAXNUMBEROFHORSE 50
#include "Version.h"
#include "SingleNum.h"


class RACECLASS:
        public SINGLE,
        public VERSION 
{
        private:
        int month;              // save month
        int year;               // save year
        int lasthorse;          // last horse in the race
        
        public:
        // Routines for Day Value
        void SetRaceDate(int RaceYear, int RaceMonth, int RaceDay);
        int GetDV(int Month, int Day);// grab DV from Parameters
        int GetDV(void);        // grab DV from race date
        AnsiString GetADV(void); // get the string of Day Value
        AnsiString GetADay(void);
        int GetMonth(void);
        AnsiString GetAMonth(void);
        int GetYear(void);
        AnsiString GetAYear(void);
        


        // Routines for Full Number of Race
        int GetLH(void) {return(lasthorse);}            // Get the Last horse from structure
        void SetLH(int HNumber) {lasthorse = HNumber;}  // set the last horse
        int GetFNH(void);       // get the FNH from Last horse
        int GetFNH(int Number);        // Get the FNH from parametr       
        AnsiString GetFNHA(void);       // Get The ascii of FNH

        // Routines for Horse name Value
        int GetHV(int HN);     // get Value of horse
        void SetHorseSt(int HNumber, AnsiString HN); // set the horse name to struct
        AnsiString GetHorseName(int HN); // get the horse name by HN


        AnsiString PreferenceOrderI(int HN);  // Preference order process 1
        AnsiString PreferenceOrderII(int HN);  // Preference order process 2
        
        // Routines for Initial Digit
        AnsiString GetIDA(void);       // Get The ascii of FNH


 
        // Routines for VP
        AnsiString GetVPA(void);
        int GetVP(void);
        void SetVP(AnsiString RName);
        AnsiString GetRName(void);
        

        //Routines for save and Open
        void SaveFile(AnsiString FileName);
        void ReadFile(AnsiString FileName);
        void ReadComaFile(AnsiString FileName);
        int ExDate(char *s);
        int ExMonth(char *s);
        int ExYear(char *s);
        char * ExRName(char *s);        //extract race name from coma
        char * ExHName(char *s);        // extract hourse name
        
                
        //Routines for best Assimulation
        int GetNameValue(AnsiString Name);


        void AddHorse(int HorseNum, AnsiString HorseName);      // Add A horse

        int GetID(void);       // ID number

        
private:
        // Routines for Numbers
        int GetEgyptValue(char Letter);  // get egyptian value
        int GetHebrewValue(char Letter); // get hebrew values
        int GetFirstLetter(AnsiString Name, int Start);    // get the first letter of string
        int IsVowel(char Letter);       // test if it is a vowel
        char GetChar(AnsiString Name, int Start);
        int IsSame(int HNumber, int HValue);  // test for Unit force
        int GetNPP1(int Start, int NH); // Get Number of Points
        int GetNPP2(int Start, int NH); // Get number of poits
        int GetFirstP1(int VH, int NH); // Get WDO
        int GetSecP1(int VH, int NH); // get Second pair of Process 1
        int GetThirdP1(int VH, int NH); // get third part of Pro1
        int GetFirstP2(int VH, int NH); // Process 2
        int GetSecP2(int VH, int NH);   // Process 2 part 2
        int GetThirdP2(int VH, int NH); // Process 2 part 3
        int GetInitalDigit(char Letter); // ID data
        int IsOW1(int HN, int HV);      // Outright Winner Pro 1
        int IsOW2(int HN, int HV);      // Outright Winner Pro 2
        int IsDF1(int HN, int HV);      // Find Duble Force pro 1
        int IsDF2(int HN, int HV);      // Find Double Force Pro 2
        int IsSameRF(int NUM);
        int IsDC(int NUM);              // Test for Direct connection
        int IsFF1(int HV, int HN);       // find the full force
        int IsUnitForce1(int HN, int HV);  // return Unit Force
        int IsUnitForce2(int HN, int HV);  //Return Unit Force Pro II
        AnsiString RFLetter(int Num);
        AnsiString RFLetterX(int Num);         //return RF letter x if not
        AnsiString SerialSel1(int HN, int HV); // serial selection Pro I
        AnsiString SerialSel2(int HN, int HV); // serial selection Pro II
        int NumberRF1(int HN, int HV);   // find the number of Ruling figures
        int NumberRF2(int HN, int HV);  // find the number RF in Pro II
         
};


extern RACECLASS RaceInfo;

#endif



