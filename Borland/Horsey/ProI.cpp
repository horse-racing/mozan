//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "HDV.h"
#include "HDVLV.h"
#include "HVP.h"
#include "HNEnter.h"
#include "Alpha.h"
#include "Serialsel.h"
#include "OutRightWin.h"
#include "UnitForce.h"
#include "ProI.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)


//**********************************************************
// Get Number test if Ruling Figure return letter representation
//**********************************************************
AnsiString PROCESSI::GetRFigure(int value)
{
        ALPHABET is;
        AnsiString letter;

        if(is.IsSame(value,FDayValue->GetDaysValue())) letter = "A";
        if(is.IsSame(value,FHorseNames->GetFNHValue())) letter = letter + "B";
        if(is.IsSame(value,FHorseNames->GetIDValue())) letter = letter + "a";
        if(is.IsSame(value,FDvlV->GetDVLV())) letter = letter + "b";
        if(is.IsSame(value,FVP->GetRNValue())) letter = letter + "c";
        if(letter.IsEmpty()) letter = IntToStr(value);
        return(letter);
}

//---------------------------------------------------------
// Get Third part Of process 1
//
int PROCESSI::GetSecPair2(int value, int number)
{
        int Points;
        ALPHABET alpha;

        Points = alpha.GetNPointsI(value,number);

        return(alpha.GetWDO(Points,number));
}


//---------------------------------------------------------
// Get second pair 1
//
int PROCESSI::GetSecPair1(int value, int number)
{
        int Points;
        ALPHABET alpha;

        Points = alpha.GetNPointsI(value,number);

        return(alpha.GetCLO(Points,number));
}



int PROCESSI::GetFirstPair(int value, int number)
{
        int Points;
        ALPHABET alpha;

        Points = alpha.GetNPointsI(value,number);

        return(alpha.GetWDO(Points,value));
}


//----------------------------------------------------------------------------
// build an ansi string of the first line of process 1
AnsiString PROCESSI::AnsiProcess(int number, int value)
{
        AnsiString process;
        AnsiString aprocess;
        AnsiString numofrf;     //text for number of ruling figures
        ALPHABET alpha;
        SERIALSELECT ss;

        process.printf("%d/%d-%d (%d) %d/%d-%d     ",value,number,GetFirstPair(value,number),alpha.GetNPointsI(value,number),
                number,GetSecPair1(value,number),GetSecPair2(value,number));
        aprocess.printf("RF--> %s/%s-%s (%s) %s/%s-%s    ",GetRFigure(value),GetRFigure(number),GetRFigure(GetFirstPair(value,number)),
                GetRFigure(alpha.GetNPointsI(value,number)),GetRFigure(number),GetRFigure(GetSecPair1(value,number)),GetRFigure(GetSecPair2(value,number)));
        process = process + aprocess + ss.SerialSel(value,number,GetFirstPair(value,number));
        numofrf.printf("    Number of R.F. = %d",HowManyRF(number,value));
        process = process + numofrf;
        return(process);
}

//----------------------------------------------------------------------------
// build an ansi string of the first line of process 1
AnsiString PROCESSI::AnsiProcess(int number, int value, bool uf)
{
        AnsiString process;
        AnsiString aprocess;
        AnsiString numofrf;     //text for number of ruling figures
        ALPHABET alpha;
        SERIALSELECT ss;

        process.printf("%d/%d-%d (%d) %d/%d-%d     ",value,number,GetFirstPair(value,number),alpha.GetNPointsI(value,number),
                number,GetSecPair1(value,number),GetSecPair2(value,number));
        aprocess.printf("RF--> %s/%s-%s (%s) %s/%s-%s    ",GetRFigure(value),GetRFigure(number),GetRFigure(GetFirstPair(value,number)),
                GetRFigure(alpha.GetNPointsI(value,number)),GetRFigure(number),GetRFigure(GetSecPair1(value,number)),GetRFigure(GetSecPair2(value,number)));
        process = process + aprocess + ss.SerialSel(value,number,GetFirstPair(value,number),uf);
        numofrf.printf("    Number of R.F. = %d",HowManyRF(number,value));
        process = process + numofrf;
        return(process);
}

//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>
// find out howmany ruling figures in this process
int PROCESSI::HowManyRF(int number,int value)
{
        SERIALSELECT ss;
        ALPHABET alpha;
        int count = 0;

        count = count + ss.CountRF(value);
        count = count + ss.CountRF(number);
        count = count + ss.CountRF(GetFirstPair(value,number));
        count = count + ss.CountRF(alpha.GetNPointsI(value,number));
        count = count + ss.CountRF(GetSecPair1(value,number));
        count = count + ss.CountRF(GetSecPair2(value,number));
        return(count);
}


//PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
// return the if this horse is UOW
bool PROCESSI::IsUOW(int number, int value)
{
        bool retval = false;

        if(IsUOWDV(number,value)) retval = true;
        if(IsUOWVH(number,value)) retval = true;

        return(retval);
}

//PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
// return the if this horse is UOW
bool PROCESSI::IsOW(int number, int value)
{
        bool retval = false;

        if(IsOWDV(number,value)) retval = true;
        if(IsOWVH(number,value)) retval = true;

        return(retval);
}



//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//fine the preference order
AnsiString PROCESSI::FindPreference(int number,int value)
{
        SERIALSELECT ss;
        ALPHABET alpha;
        UNITFORCE uf;
        AnsiString result;
        bool found = false;

        if(IsUOWDV(number,value))
        {
                result = "U.O.W. at the same number of points as D.V.";
                found = true;
        }
        else if(IsUOWVH(number,value))
        {
                result = "U.O.W. at the same number of points as V.H.";
                found = true;
        }
        
        if(!found)
        {
                if(IsOWDV(number,value))
                {
                        result = "O.W. at the same number of points as D.V.";
                        found = true;
                }
                else if(IsOWVH(number,value))
                {
                        result = "O.W. at the same number of points as V.H.";
                        found = true;
                }
        }

        if(!found)
        {
                if(IsUF(number,value))
                {
                        result = "Unit Force";
                        found = true;
                }
        }

        if(!found)
        {
                if(IsDF(number,value))
                {
                        result = "Double Force";
                        found = true;
                }
        }

        if(!found)
        {
        	if(IsSFF(number,value))
                {
                	result = "Special Full Force";
                        found = true;
                }
                else if(IsOrdFF(number,value))
                {
                	result = "Ordinary Full Force";
                        found = true;
                }
                else if(IsFF(number,value))
                {
                        result = "Full Force";
                        found = true;
                }
        }

        return(result);


}




//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// test if we have a Outright Winner with day value
bool PROCESSI::IsOWDV(int number, int value)
{
        SERIALSELECT ss;
        ALPHABET alpha;
        UNITFORCE uf;
        
        bool result = false;

        if(!alpha.IsSame(number,value))
        {
                if(ss.Is_DV(value,number,GetFirstPair(value,number)))
                {
                        if(ss.Is_FNH(value,number,GetFirstPair(value,number)))
                        {
                                if(ss.Is_ID(value,number,GetFirstPair(value,number)))
                                {
                                        if(alpha.IsSame(FDayValue->GetDaysValue(),alpha.GetNPointsI(value,number)))
                                        {
                                                result = true;
                                        }
                                }
                                if(ss.Is_VP(value,number,GetFirstPair(value,number)))
                                {
                                        if(alpha.IsSame(FDayValue->GetDaysValue(),alpha.GetNPointsI(value,number)))
                                        {
                                                result = true;
                                        }
                                }
                        }
                }
        }
        return(result);
}

//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// test if we have a Outright Winner with value of horse
bool PROCESSI::IsOWVH(int number, int value)
{
        SERIALSELECT ss;
        ALPHABET alpha;
        UNITFORCE uf;
        bool result = false;

        if(!alpha.IsSame(number,value))
        {
                if(ss.Is_DV(value,number,GetFirstPair(value,number)))
                {
                        if(ss.Is_FNH(value,number,GetFirstPair(value,number)))
                        {
                                if(ss.Is_ID(value,number,GetFirstPair(value,number)))
                                {
                                        if(alpha.IsSame(value,alpha.GetNPointsI(value,number)))
                                        {
                                                result = true;
                                        }
                                }
                                if(ss.Is_VP(value,number,GetFirstPair(value,number)))
                                {
                                        if(alpha.IsSame(value,alpha.GetNPointsI(value,number)))
                                        {
                                                result = true;
                                        }
                                }
                        }
                }
        }
        return(result);
}


//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// test if we have a Unit Force Outright Winner with day value
bool PROCESSI::IsUOWDV(int number, int value)
{
        SERIALSELECT ss;
        ALPHABET alpha;
        UNITFORCE uf;
        bool result = false;

        if(alpha.IsSame(number,value))
        {
                number = uf.ModifyUForce(number,value);
                if(ss.Is_DV(value,number,GetFirstPair(value,number)))
                {
                        if(ss.Is_FNH(value,number,GetFirstPair(value,number)))
                        {
                                if(ss.Is_ID(value,number,GetFirstPair(value,number)))
                                {
                                        if(alpha.IsSame(FDayValue->GetDaysValue(),alpha.GetNPointsI(value,number)))
                                        {
                                                result = true;
                                        }
                                }
                                if(ss.Is_VP(value,number,GetFirstPair(value,number)))
                                {
                                        if(alpha.IsSame(FDayValue->GetDaysValue(),alpha.GetNPointsI(value,number)))
                                        {
                                                result = true;
                                        }
                                }
                        }
                }
        }
        return(result);
}


//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// test if we have a Unit Force Outright Winner with value of horse
bool PROCESSI::IsUOWVH(int number, int value)
{
        SERIALSELECT ss;
        ALPHABET alpha;
        UNITFORCE uf;
        bool result = false;

        if(alpha.IsSame(number,value))
        {
                number = uf.ModifyUForce(number,value);
                if(ss.Is_DV(value,number,GetFirstPair(value,number)))
                {
                        if(ss.Is_FNH(value,number,GetFirstPair(value,number)))
                        {
                                if(ss.Is_ID(value,number,GetFirstPair(value,number)))
                                {
                                        if(alpha.IsSame(value,alpha.GetNPointsI(value,number)))
                                        {
                                                result = true;
                                        }
                                }
                                if(ss.Is_VP(value,number,GetFirstPair(value,number)))
                                {
                                        if(alpha.IsSame(value,alpha.GetNPointsI(value,number)))
                                        {
                                                result = true;
                                        }
                                }
                        }
                }
        }
        return(result);
}



//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// is this a real unit force
bool PROCESSI::IsUF(int number,int value)
{
        ALPHABET alpha;
        SERIALSELECT ss;
        UNITFORCE uf;
        bool result = false;

        if(alpha.IsSame(number,value))
        {
                number = uf.ModifyUForce(number,value);
                if(ss.Is_DV(number,GetFirstPair(value,number)))
                {
                        if(ss.Is_FNH(number,GetFirstPair(value,number)))
                        {
                                if(ss.Is_ID(GetSecPair1(value,number),GetSecPair2(value,number)))
                                {
                                        result = true;
                                }
                                if(ss.Is_DVL(GetSecPair1(value,number),GetSecPair2(value,number)))
                                {
                                        result = true;
                                }
                                if(ss.Is_VP(GetSecPair1(value,number),GetSecPair2(value,number)))
                                {
                                        result = true;
                                }
                        }
                }
                if(!result)
                {
                        if(ss.Is_DV(number,GetFirstPair(value,number)))
                        {
                                if(ss.Is_FNH(number,GetFirstPair(value,number)))
                                {
                                        if(ss.Is_ID(GetSecPair1(value,number),GetSecPair2(value,number)))
                                        {
                                                if(ss.Is_VP(GetSecPair1(value,number),GetSecPair2(value,number)))
                                                {
                                                        result = true;
                                                }
                                                else if(ss.Is_DVL(GetSecPair1(value,number),GetSecPair2(value,number)))
                                                {
                                                        result = true;
                                                }
                                        }
                                }
                                else if(ss.Is_ID(number,GetFirstPair(value,number)))
                                {
                                        if(ss.Is_FNH(GetSecPair1(value,number),GetSecPair2(value,number)))
                                        {
                                                if(ss.Is_VP(GetSecPair1(value,number),GetSecPair2(value,number)))
                                                {
                                                        result = true;
                                                }
                                                else if(ss.Is_DVL(GetSecPair1(value,number),GetSecPair2(value,number)))
                                                {
                                                        result = true;
                                                }
                                        }
                                }
                                else if(ss.Is_DVL(number,GetFirstPair(value,number)))
                                {
                                        if(ss.Is_FNH(GetSecPair1(value,number),GetSecPair2(value,number)))
                                        {
                                                if(ss.Is_VP(GetSecPair1(value,number),GetSecPair2(value,number)))
                                                {
                                                        result = true;
                                                }
                                                else if(ss.Is_ID(GetSecPair1(value,number),GetSecPair2(value,number)))
                                                {
                                                        result = true;
                                                }
                                        }
                                }
                                else if(ss.Is_VP(number,GetFirstPair(value,number)))
                                {
                                        if(ss.Is_FNH(GetSecPair1(value,number),GetSecPair2(value,number)))
                                        {
                                                if(ss.Is_DVL(GetSecPair1(value,number),GetSecPair2(value,number)))
                                                {
                                                        result = true;
                                                }
                                                else if(ss.Is_ID(GetSecPair1(value,number),GetSecPair2(value,number)))
                                                {
                                                        result = true;
                                                }
                                        }
                                }
                        }
                }
        }
        return(result);
}


//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// is this a double force
bool PROCESSI::IsDF(int number,int value)
{
        ALPHABET alpha;
        SERIALSELECT ss;
        UNITFORCE uf;
        bool result = false;

        if(!IsUF(number,value))
        {
                number = uf.ModifyUForce(number,value);
                if(ss.Is_DV(GetFirstPair(value,number)))
                {
                        if(ss.Is_FNH(number))
                        {
                                if(ss.Is_ID(value))
                                {
                                        if(ss.Is_DVL(GetSecPair1(value,number),GetSecPair2(value,number)))
                                        {
                                                result = true;
                                        }
                                        if(ss.Is_VP(GetSecPair1(value,number),GetSecPair2(value,number)))
                                        {
                                                result = true;
                                        }
                                }
                        }
                }
                else if(ss.Is_FNH(GetFirstPair(value,number)))
                {
                        if(ss.Is_DV(number))
                        {
                                if(ss.Is_ID(GetSecPair1(value,number),GetSecPair2(value,number)))
                                {
                                        if(ss.Is_VP(GetSecPair1(value,number),GetSecPair2(value,number)))
                                        {
                                                result = true;
                                        }
                                        else if(ss.Is_DVL(GetSecPair1(value,number),GetSecPair2(value,number)))
                                        {
                                                result = true;
                                        }
                                }
                        }
                }
        }
        return(result);
}

//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// is this a double force
bool PROCESSI::IsFF(int number,int value)
{
        ALPHABET alpha;
        SERIALSELECT ss;
        UNITFORCE uf;
        bool result = false;

        if(!IsUF(number,value))
        {
                number = uf.ModifyUForce(number,value);
                if(ss.Is_FNH(GetFirstPair(value,number),number))
                {
                	if(ss.Is_DV(GetSecPair1(value,number),GetSecPair2(value,number)))
                        {
                		result = true;
                        }
                }
                else if(ss.Is_DV(GetFirstPair(value,number),number))
                {
                        if(ss.Is_FNH(GetSecPair1(value,number),GetSecPair2(value,number)))
                        {
                                result = true;
                        }
                }
        }
        return(result);
}
//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// is this a Special full force
bool PROCESSI::IsSFF(int number,int value)
{
        ALPHABET alpha;
        SERIALSELECT ss;
        UNITFORCE uf;
        bool result = false;

        if(!IsUF(number,value))
        {
                number = uf.ModifyUForce(number,value);

                if(ss.Is_ID(number,GetFirstPair(value,number)))
                {
                	if(ss.Is_FNH(number,GetFirstPair(value,number)))
                        {
                        	if(ss.Is_DV(GetSecPair1(value,number),GetSecPair2(value,number)))
	                        {
                                                result = true;
                                }
                        }
                        else if(ss.Is_DV(GetSecPair1(value,number),GetSecPair2(value,number)))
                        {
                                result = true;
                        }
                }
        }
        return(result);
}

//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// is this a double force
bool PROCESSI::IsOrdFF(int number,int value)
{
        ALPHABET alpha;
        SERIALSELECT ss;
        UNITFORCE uf;
        bool result = false;

        if(!IsUF(number,value))
        {
                number = uf.ModifyUForce(number,value);

                if(ss.Is_ID(number,GetFirstPair(value,number)))
                {
			if(ss.Is_DV(number,GetFirstPair(value,number)))
        	        {
				if(ss.Is_FNH(GetSecPair1(value,number),GetSecPair2(value,number)))
				{
                        		result = true;
	                        }
                        }
                        else if(ss.Is_FNH(GetSecPair1(value,number),GetSecPair2(value,number)))
                        {
                                result = true;
                        }
                }
        }
        return(result);
}

//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// exceptionI
bool PROCESSI::Exceptions(int number,int value)
{
        SERIALSELECT ss;
        UNITFORCE uf;
        bool result = false;

        number = uf.ModifyUForce(number,value);
        if(ss.Is_DV(GetFirstPair(value,number)))
        {
                result = true;
        }

        return(result);
}
