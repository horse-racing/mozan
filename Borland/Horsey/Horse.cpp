//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Horse.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)


HORSENAME horse[30];


//-------------------------------------------------------------
// constructor
HORSENAME::HORSENAME(void)
{
        horselist = new TStringList;
}

//-------------------------------------------------------------
// destructor
HORSENAME::~HORSENAME(void)
{
        if(horselist) delete horselist;
        horselist = 0;
}

//--------------------------------------------------------
// Description: Get Horse Name Value
//
int HORSENAME::Get_Value(void)
{
        int namelen, Value, x, NumAdd;
        char first;
        AnsiString Temp;

        namelen=name.Length();

        NumAdd=Get_First_Value(name,1);

        for(x=2;x<=namelen;x++)
        {
                first=Get_Char(name,x);
                Value = Get_Egypt_Value(first);
                if(!Value)
                {
                        x++;
                        NumAdd += Get_First_Value(name,x);

                }
                NumAdd += Value;
                NumAdd = single(NumAdd);
        }

        return NumAdd;

}

//<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// Description: Set the horse name with the number
// Arguments:   Name of horse and number of the horse
// Returns:
// Created:     28/9/2003
// Notes:
//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
void HORSENAME::Set_Name(AnsiString horsename, int horsenumber)
{
        name = horsename;
        number = horsenumber;
}
