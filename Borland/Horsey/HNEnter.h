//---------------------------------------------------------------------------

#ifndef HNEnterH
#define HNEnterH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Grids.hpp>
#include <Menus.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <Dialogs.hpp>
//---------------------------------------------------------------------------
class TFHorseNames : public TForm
{
__published:	// IDE-managed Components
        TEdit *Ehorse;
        TListBox *ListHorseHis;
        TEdit *Ehvalue;
        TCheckBox *CheckBox1;
        TButton *BHisToHor;
        TButton *Button2;
        TButton *Button3;
        TButton *BTransfer;
        TListBox *ListRHorse;
        TLabel *Label1;
        TEdit *Edit3;
        TButton *BListDel;
        TButton *Button5;
        TButton *BMup;
        TButton *BMdwn;
        TMainMenu *MainMenu1;
        TMenuItem *F1;
        TStatusBar *StatusBar1;
        TOpenDialog *OpenDialog1;
        TButton *Button1;
        void __fastcall EhorseChange(TObject *Sender);
        void __fastcall Edit1KPress(TObject *Sender, char &Key);
        void __fastcall CheckBox1Click(TObject *Sender);
        void __fastcall BHisToHorClick(TObject *Sender);
        void __fastcall Button2Click(TObject *Sender);
        void __fastcall Button3Click(TObject *Sender);
        void __fastcall ListRHorseClick(TObject *Sender);
        void __fastcall FormShow(TObject *Sender);
        void __fastcall Button5Click(TObject *Sender);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
        void __fastcall BMupClick(TObject *Sender);
        void __fastcall BMdwnClick(TObject *Sender);
        void __fastcall BListDelClick(TObject *Sender);
        void __fastcall BTransferClick(TObject *Sender);
        void __fastcall Button1Click(TObject *Sender);
private:	// User declarations

public:		// User declarations
        __fastcall TFHorseNames(TComponent* Owner);
        int GetFNHValue(void);
        int GetIDValue(void);
        int GetInitalDigit(char Letter);
        int Count(void);        // return number of horses
        AnsiString HorseName(int number);        // return horse number name
        int HorseValue(int number);          // return horse value by number
        int HorseValue(AnsiString AString);
        AnsiString ProIFPair(int number);       // Return First pair of Process I
        AnsiString ProIIFPair(int number);      // return First pair of process II
        AnsiString FindDC(int number);          // direct conection
        void AddHorse(char *horsename);
        void ClearHorse(void);
        int TotalRF(int number);                // total number of RF in processes
        AnsiString PrefProI(int number);      // returns a string of prefference order
        AnsiString PrefProII(int number);       // returns a string of pref order
        AnsiString Exceptions(int number);       // get the exception if one available
        void ExHName(char *s);
        bool Pro1IsUOW(int number);
        bool Pro2IsUOW(int number);
        bool Pro1IsOW(int number);
        bool Pro2IsOW(int number);
        bool Pro1IsUF(int number);
        bool Pro2IsUF(int number);
        bool Pro1IsDF(int number);
        bool Pro2IsDF(int number);
        bool Pro1IsFF(int number);
        bool Pro2IsFF(int number);
        bool IsException(int number);
};
//---------------------------------------------------------------------------
extern PACKAGE TFHorseNames *FHorseNames;
//---------------------------------------------------------------------------
#endif
