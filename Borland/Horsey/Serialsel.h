//---------------------------------------------------------------------------

#ifndef SerialselH
#define SerialselH
//---------------------------------------------------------------------------

class SERIALSELECT
{
private:
        AnsiString GetRFigurex(int value);

public:
        AnsiString SerialSel(int value, int number, int profpair);
        AnsiString SerialSel(int value, int number, int profpair, bool uf);
        bool IsSameRF(int number);
        int CountRF(int number);
        bool IsDV(int value,int number,int found);      // test the three number match dv
        bool Is_ChapV(int value);
        int Is_DC(int number, int value);
        bool Is_DV(int value,int number,int found);     // days value
        bool Is_DV(int number,int found);     // days value
        bool Is_DV(int number);     // days value
        bool Is_FNH(int value,int number,int found);    //Full number of horses
        bool Is_FNH(int number,int found);    //Full number of horses
        bool Is_FNH(int number);    //Full number of horses
        bool Is_ID(int value,int number,int found);     // Inital digit
        bool Is_ID(int number,int found);     // Inital digit
        bool Is_ID(int number);     // Inital digit
        bool Is_DVL(int value,int number,int found);    // dvisional value
        bool Is_DVL(int number,int found);    // dvisional value
        bool Is_DVL(int number);    // dvisional value
        bool Is_VP(int value,int number,int found);     // value of the plate
        bool Is_VP(int number,int found);     // value of the plate
        bool Is_VP(int number);     // value of the plate        
};

#endif
