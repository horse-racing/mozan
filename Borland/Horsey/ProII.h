//---------------------------------------------------------------------------

#ifndef ProIIH
#define ProIIH
//---------------------------------------------------------------------------


class PROCESSII
{
private:
        int GetFirstPair(int value, int number);
        int GetSecPair1(int value, int number);
        int GetSecPair2(int value, int number);
        AnsiString GetRFigure(int value);
        bool IsUOWDV(int number,int value);
        bool IsUOWVH(int number,int value);
        bool IsOWDV(int number,int value);
        bool IsOWVH(int number,int value);



public:
        AnsiString AnsiProcess(int number, int value); // build a line 1 string
        AnsiString AnsiProcess(int number, int value, bool uf); // build a line 1 string
        int HowManyRF(int number,int value);
        AnsiString FindPreference(int number,int value);
        bool Exceptions(int number,int value);
        bool IsUOW(int number, int value);
        bool IsOW(int number, int value);
        bool IsUF(int number,int value);
        bool IsDF(int number,int value);
        bool IsFF(int number,int value);
        bool IsSFF(int number,int value);
        bool IsOrdFF(int number,int value);
};
#endif
