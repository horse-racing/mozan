//---------------------------------------------------------------------------
#ifndef HDVH
#define HDVH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
//---------------------------------------------------------------------------
class TFDayValue : public TForm
{
__published:	// IDE-managed Components
        TMonthCalendar *MonthCalendar1;
        TEdit *EDValue;
        TLabel *Label2;
        TButton *Button1;
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall EDValueRefresh(TObject *Sender);
        void __fastcall Button1Click(TObject *Sender);
        void __fastcall SetDateStruct(TObject *Sender);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
        
        
private:	// User declarations
        Word wYear;
        Word wMonth;
        Word wDay;

public:		// User declarations
        __fastcall TFDayValue(TComponent* Owner);
        AnsiString GetDateStr(void);
        int GetDaysValue(void);
        void SetDate(Word date) {wDay = date;MonthCalendar1->Date = EncodeDate(wYear, wMonth, wDay);}
        void SetMonth(Word month) {wMonth = month;MonthCalendar1->Date = EncodeDate(wYear, wMonth, wDay);}
        void SetYear(Word year) {wYear = year;MonthCalendar1->Date = EncodeDate(wYear, wMonth, wDay);}
        int GetDate(void) {DecodeDate(MonthCalendar1->Date, wYear, wMonth, wDay); return(wDay);}
        int GetMonth(void) {DecodeDate(MonthCalendar1->Date, wYear, wMonth, wDay); return(wMonth);}
        int GetYear(void) {DecodeDate(MonthCalendar1->Date, wYear, wMonth, wDay); return(wYear);}
        int ExDate(char *s);
        int ExMonth(char *s);
        int ExYear(char *s);        
};
//---------------------------------------------------------------------------
extern PACKAGE TFDayValue *FDayValue;
//---------------------------------------------------------------------------


#endif
