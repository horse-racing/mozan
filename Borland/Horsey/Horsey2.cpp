//---------------------------------------------------------------------------
#include <vcl.h>
#include <winver.h>
#include <string.h>
#include <stdio.h>
#include <dir.h>
#pragma hdrstop


#include "Horsey2.h"
#define MAXNUMOFHORSE 30
RACECLASS RaceInfo;

//---------------------------------------------------------------------------
#pragma package(smart_init)

typedef struct
{
        AnsiString Version;            // store vertion
        AnsiString HNames[MAXNUMOFHORSE];      // Horse names stored here
        AnsiString RaceName;  // The race name stored here
}RACESTRUCT;

RACESTRUCT RaceData;

int Score[30];










//---------------------------------------------------------
// Get char from Ansistring
//
char RACECLASS::GetChar(AnsiString Name, int Start)
{
        AnsiString Temp;
        char *ch;

        Temp = Name.SubString(Start,1);
        ch = Temp.c_str();
        return ch[0];
}

//---------------------------------------------------------
// Get Number of Points Chaldean order
//
int RACECLASS::GetNPP1(int Start, int NH)
{
        int x,a;

        for(x=1;x<10;x++)
        {
//                a=ChaldeanOrder[Start][x];
                if(IsSame(a,NH))
                {
                        return x;
                }
        }
        return 0;

}


//---------------------------------------------------------
// Get Number of Points Week Day Order order
//
int RACECLASS::GetNPP2(int Start, int NH)
{
        int x,a;

        for(x=1;x<10;x++)
        {
//                a=WeekDayOrder[Start][x];
                if(IsSame(a,NH))
                {
                        return x;
                }
        }
        return 0;

}
//---------------------------------------------------------
// Get the Week Day order from Points
//
int RACECLASS::GetFirstP1(int VH, int NH)
{
        int Points;

        Points=GetNPP1(VH,NH);

        return 0;
//         WeekDayOrder[VH][Points];
}

//---------------------------------------------------------
// Get second pair 1
//
int RACECLASS::GetSecP1(int VH, int NH)
{
        int Points;

        Points=GetNPP1(VH,NH);

        return 0;
//         ChaldeanOrder[NH][Points];
}

//---------------------------------------------------------
// Get Third part Of process 1
//
int RACECLASS::GetThirdP1(int VH, int NH)
{
        int Points;

        Points=GetNPP1(VH,NH);

        return 0;
//         WeekDayOrder[NH][Points];
}

//--------------------------------------------------------------
//Get Thirt Part of process 2
//
int RACECLASS::GetFirstP2(int VH, int NH)
{
        int Points;

        Points=GetNPP2(VH,NH);

        return 0;
//         ChaldeanOrder[VH][Points];
}

//--------------------------------------------------------------
// Get Second part of Process 2
//
int RACECLASS::GetSecP2(int VH, int NH)
{
        int Points;

        Points=GetNPP2(VH,NH);

        return 0;
//         ChaldeanOrder[NH][Points];
}

//---------------------------------------------------------------
// Get Third Part of Process 2
//
int RACECLASS::GetThirdP2(int VH, int NH)
{
        int Points;

        Points=GetNPP2(VH,NH);

        return 0;
//         WeekDayOrder[NH][Points];
}



//--------------------------------------------------------
// Get the first Letter of String value
// Chapter III page 5 How to fine the name value
// If the following or the second letter of that word is
// a Vowel A,E,I,O,U,Y ??? use the Hebrew numbers.
// else use Egyptian Numbers.
//-----------------------------------------------------------
int RACECLASS::GetFirstLetter(AnsiString Name, int Start)
{
        char first, second;
        int Value;

        Value=0;
        first=GetChar(Name,Start);

        switch(first)
        {
                case 'c':
                case 'C':
                case 's':
                case 'S':
                        second = GetChar(Name,Start+1);
                        if(IsVowel(second))
                        {
                                Value +=GetHebrewValue(first);
                        }
                        else
                        {
                                Value +=GetEgyptValue(first);
                        }
                        return Value;

                case 'o':
                case 'O':
                case 'r':
                case 'R':
                        Value +=GetHebrewValue(first);
                        return Value;
                default:
                        Value +=GetEgyptValue(first);
                        return Value;
        }
}

//--------------------------------------------------------
// Get the FNH from Last Horse
//
int RACECLASS::GetFNH(void)
{
        return(single(lasthorse));
}

//--------------------------------------------------------
// Get the FNH from Parameter
//
int RACECLASS::GetFNH(int Number)
{
        return(single(Number));
}

//--------------------------------------------------------
// Get Ascii of FNH
//
AnsiString RACECLASS::GetFNHA(void)
{
        return(IntToStr(GetFNH()));
}

//--------------------------------------------------------
// Get Horse Name Value
//
int RACECLASS::GetHV(int HN)
{
        int NameLen, Value, x, NumAdd;
        char first;
        AnsiString Name,Temp;

        Name=GetHorseName(HN);  // get horse name from stucture
        //HN=NumoUno(HN);
        //NumAdd=0;
        NameLen=Name.Length();

        NumAdd=GetFirstLetter(Name,1);

        for(x=2;x<=NameLen;x++)
        {
                first=GetChar(Name,x);
                Value = GetEgyptValue(first);
                if(!Value)
                {
                        x++;
                        NumAdd += GetFirstLetter(Name,x);

                }
                NumAdd += Value;
                NumAdd = single(NumAdd);
        }

        return NumAdd;


}

//--------------------------------------------------------
// Get a progressive Name Value
//-------------------------------------------------------
int RACECLASS::GetNameValue(AnsiString Name)
{
        int NameLen, Value, x, NumAdd;
        char first;
        AnsiString Temp;

        NameLen=Name.Length();
        NumAdd=GetFirstLetter(Name,1);

        for(x=2;x<=NameLen;x++)
        {
                first=GetChar(Name,x);
                Value = GetEgyptValue(first);
                if(!Value)
                {
                        x++;
                        NumAdd += GetFirstLetter(Name,x);

                }
                NumAdd += Value;
                NumAdd = single(NumAdd);
        }

        return NumAdd;


}


//--------------------------------------------------------
// Get letter Value
//
int RACECLASS::GetEgyptValue(char Letter)
{
        switch (Letter)
        {
                case 'a': case 'A':
                case 'i': case 'I':
                case 'y': case 'Y':
                        return 1;
                case 'b': case 'B':
                case 'k': case 'K':
                        return 2;
                case 'c': case 'C':
                case 'g': case 'G':
                case 'j': case 'J':
                case 'l': case 'L':
                case 'q': case 'Q':
                        return 3;
                case 'd': case 'D':
                case 'm': case 'M':
                case 'r': case 'R':
                case 't': case 'T':
                        return 4;
                case 'e': case 'E':
                case 'n': case 'N':
                        return 5;
                case 's': case 'S':
                case 'u': case 'U':
                case 'v': case 'V':
                case 'w': case 'W':
                        return 6;
                case 'x': case 'X':
                case 'z': case 'Z':
                        return 7;
                case 'f': case 'F':
                case 'h': case 'H':
                case 'o': case 'O':
                case 'p': case 'P':
                        return 8;
                default:
                        return 0;
        }


}
//--------------------------------------------------------
// get the hebrew values
//
int RACECLASS::GetHebrewValue(char Letter)
{
        switch (Letter)
        {
                case 'c':
                case 'C':
                case 'r':
                case 'R':
                        return 2;
                case 'o':
                case 'O':
                        return 7;
                case 's':
                case 'S':
                        return 3;
                default:
                        return 0;
        }
}








//---------------------------------------------------------
// Thest for Unit force
//
int RACECLASS::IsSame(int HNumber, int HValue)
{

        if(HNumber == HValue) return -1;
        if((HNumber == 1) && (HValue == 4)) return -1;
        if((HNumber == 4) && (HValue == 1)) return -1;

        if((HNumber == 2) && (HValue == 7)) return -1;
        if((HNumber == 7) && (HValue == 2)) return -1;

        return 0;
}

//----------------------------------------------------------
// Is char a Vowel
//
int RACECLASS::IsVowel(char Letter)
{
        switch(Letter)
        {
                case 'a':
                case 'A':
                case 'e':
                case 'E':
                case 'i':
                case 'I':
                case 'o':
                case 'O':
                case 'u':
                case 'U':
                case 'y':
                case 'Y':
                        return -1;
                default:
                        return 0;
        }
}






//--------------------------------------------------------
//ssssssssssssssssssssssssssssssssssssssssssssssssssssssss
//--------------------------------------------------------

//---------------------------------------------------------
// Routen to save race date.
//
void RACECLASS::SetRaceDate(int RaceYear, int RaceMonth, int RaceDay)
{
        year = RaceYear;
        month = RaceMonth;
}


//--------------------------------------------------------
// Get letter Value
//
int RACECLASS::GetInitalDigit(char Letter)
{
        switch (Letter)
        {
                case 'a': case 'A':
                case 'i': case 'I':
                case 'y': case 'Y':
                        return 1;
                case 'b': case 'B':
                case 'k': case 'K':
                case 'r': case 'R':
                        return 2;
                case 'c': case 'C':
                case 'g': case 'G':
                case 'j': case 'J':
                case 'l': case 'L':
                case 'q': case 'Q':
                        return 3;
                case 'd': case 'D':
                case 'm': case 'M':
                case 't': case 'T':
                        return 4;
                case 'e': case 'E':
                case 'n': case 'N':
                        return 5;
                case 's': case 'S':
                case 'u': case 'U':
                case 'v': case 'V':
                case 'w': case 'W':
                        return 6;
                case 'x': case 'X':
                case 'z': case 'Z':
                        return 7;
                case 'f': case 'F':
                case 'h': case 'H':
                case 'o': case 'O':
                case 'p': case 'P':
                        return 8;
                default:
                        return 0;
        }


}


//----------------------------------------------------------
// Routines for Inital Digit
//
int RACECLASS::GetID(void)
{
        int x,NumAdd;
        AnsiString Horse;
        char ch;

        NumAdd=0;
        for(x=1; x<=lasthorse; x++)
        {
                Horse=RaceData.HNames[x];
                ch=GetChar(Horse,1);
                NumAdd+=GetInitalDigit(ch);
                NumAdd = single(NumAdd);
        }
        return NumAdd;
}

//----------------------------------------------------------
// Get acsi ID
//
AnsiString RACECLASS::GetIDA(void)
{
        return IntToStr(GetID());

}


//******************************************************
// Routine to Value of Plate
//******************************************************


//
AnsiString RACECLASS::GetRName(void)
{
        return RaceData.RaceName;
}


//-----------------------------------------------------------
// Get The Ascii Value Race Name Structure
//
AnsiString RACECLASS::GetVPA(void)
{

        int a;

        a = GetVP();

        return IntToStr(a);
}

//---------------------------------------------------------
// Get VP
//
int RACECLASS::GetVP(void)
{

        int NameLen, Value, x, NumAdd;
        char first;
        AnsiString Name,Temp;

        Name=RaceData.RaceName;  // get horse name from stucture

        //NumAdd=0;
        NameLen=Name.Length();

        NumAdd=GetFirstLetter(Name,1);

        for(x=2;x<=NameLen;x++)
        {
                first=GetChar(Name,x);
                Value = GetEgyptValue(first);
                if(!Value)
                {
                        x++;
                        NumAdd += GetFirstLetter(Name,x);

                }
                NumAdd += Value;
                NumAdd = single(NumAdd);
        }

        return NumAdd;

}


//-----------------------------------------------------------
// Set Race Name to Structure
//
void RACECLASS::SetVP(AnsiString RName)
{

        RaceData.RaceName = RName;
}






//*************************************
char * RACECLASS::ExHName(char *s)
{
        char *RetPtr;

        while(*s != ',') s++;
        s++;
        while(*s != ',') s++;
        s++;
        while(*s != ',') s++;
        s++;
        RetPtr=s;
        while((*s != ',') && (*s != '(')) s++;

        *s=0;
        return(RetPtr);
}


//***********************************
char * RACECLASS::ExRName(char *s)
{
        char *RetPtr;

        while(*s != ',') s++;
        s++;
        RetPtr=s;
        while(*s != ',') s++;

        *s=0;
        return(RetPtr);
}


int RACECLASS::ExDate(char *s)
{
        char Buff[256];
        int x;

        x=0;
        while(*s != '.')
        {
                Buff[x]=*s;
                s++;
                x++;
        }
        Buff[x]=0;
        return(atoi(Buff));
}

int RACECLASS::ExMonth(char *s)
{
        char Buff[256];
        int x;

        x=0;
        while(*s != '.') s++;
        s++;
        while(*s != '.')
        {
                Buff[x]=*s;
                s++;
                x++;
        }
        Buff[x]=0;
        return(atoi(Buff));
}

int RACECLASS::ExYear(char *s)
{
        char Buff[256];
        int x;

        x=0;
        while(*s != '.') s++;
        s++;
        while(*s != '.') s++;
        s++;
        while(*s != ',')
        {
                Buff[x]=*s;
                s++;
                x++;
        }
        Buff[x]=0;
        return(atoi(Buff));
}




//********************************************************
// Routines to do day and Month and Year
// and The Day Value
//********************************************************

//----------------------------------------------------------
// Go and get the day value
//
int RACECLASS::GetDV(int Month, int Day)
{

}

//--------------------------------------------------------
// An over load to get day value from structure
//
int RACECLASS::GetDV(void)
{

}

//---------------------------------------------------------
// Get the string value of Day Value
//
AnsiString RACECLASS::GetADV(void)
{
        AnsiString AS;

        AS=IntToStr(GetDV());
        return AS;
}






//--------------------------------------------------------
// get Month fro struct
//
int RACECLASS::GetMonth(void)
{
        return(month);
}

//---------------------------------------------------------
// return Ascii Month
//
AnsiString RACECLASS::GetAMonth(void)
{
        return IntToStr(GetMonth());
}

//------------------------------------------------------
// Get Year from struct
//
int RACECLASS::GetYear(void)
{
        return(year);
}

//------------------------------------------------------
// Return the Asci Year
//
AnsiString RACECLASS::GetAYear(void)
{

        return IntToStr(GetYear());
}







//***************************************************
// Routines for Horse name
//***************************************************
AnsiString RACECLASS::GetHorseName(int HN)
{
        return RaceData.HNames[HN];
}


//-----------------------------------------------------------
// Save the horse name
//
void RACECLASS::SetHorseSt(int HNumber, AnsiString HN)
{
        RaceData.HNames[HNumber]=HN;
}



//**********************************
// Test for Direct Connection
//**********************************
int RACECLASS::IsDC(int NUM)
{
        int temp;
        
        temp=0;
        if(NUM == 8) temp=temp+IsSameRF(3);
        if(NUM == 9) temp=temp+IsSameRF(2);
        if(NUM == 9) temp=temp+IsSameRF(7);
        temp=temp+IsSameRF(NUM);

        return(temp);
}







