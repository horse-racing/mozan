//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
USEFORM("horsey.cpp", MainForm1);
USEFORM("HDV.cpp", FDayValue);
USEFORM("HDVLV.cpp", FDvlV);
USEFORM("HVP.cpp", FVP);
USEFORM("HNEnter.cpp", FHorseNames);
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
        try
        {
                 Application->Initialize();
                 Application->Title = "Horse program";
                 Application->CreateForm(__classid(TMainForm1), &MainForm1);
                 Application->CreateForm(__classid(TFDayValue), &FDayValue);
                 Application->CreateForm(__classid(TFDvlV), &FDvlV);
                 Application->CreateForm(__classid(TFVP), &FVP);
                 Application->CreateForm(__classid(TFHorseNames), &FHorseNames);
                 Application->Run();
        }
        catch (Exception &exception)
        {
                 Application->ShowException(&exception);
        }
        return 0;
}
//---------------------------------------------------------------------------
