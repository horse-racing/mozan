//---------------------------------------------------------------------------
#include <vcl.h>
#include <inifiles.hpp>
#pragma hdrstop

#include "HVP.h"
#include "HDVLV.h"
#include "Horsey2.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFDvlV *FDvlV;
//---------------------------------------------------------------------------
__fastcall TFDvlV::TFDvlV(TComponent* Owner)
        : TForm(Owner)
{
        TIniFile *ini;

        ini = new TIniFile(ChangeFileExt(Application->ExeName,".INI"));
        Top = ini->ReadInteger("DivForm","Top",10);
        Left = ini->ReadInteger("DivForm","Left",10);
        
        delete ini;
}
//---------------------------------------------------------------------------
void __fastcall TFDvlV::FormCreate(TObject *Sender)
{
        Edit1->Text = ScrollBar1->Position;
                
}
//---------------------------------------------------------------------------
void __fastcall TFDvlV::Ed1Exit(TObject *Sender)
{
        int a;

        if(Edit1->Text != "") a=StrToInt(Edit1->Text);

        if(a > ScrollBar1->Max)
        {
                a=ScrollBar1->Max;
        }
        if(a < ScrollBar1->Min)
        {
                a=ScrollBar1->Min;
        }

        Edit1->Text=IntToStr(a);
        ScrollBar1->Position=a;
        
}
//---------------------------------------------------------------------------
void __fastcall TFDvlV::ScrollChange(TObject *Sender)
{
        Edit1->Text = ScrollBar1->Position;        
}
//---------------------------------------------------------------------------
void __fastcall TFDvlV::Ed1Change(TObject *Sender)
{
        Edit2->Text = GetDVLV();//ScrollBar1->Position);
}
//---------------------------------------------------------------------------
void __fastcall TFDvlV::Button1Click(TObject *Sender)
{
//        RaceInfo.SetDiv(ScrollBar1->Position);
        FDvlV->Hide();
}
//---------------------------------------------------------------------------

//----------------------------------------------------------
// Get Dvl.V from Struct
//
int TFDvlV::GetDVLV(void)
{
        int division = 0;
        SINGLE singles;

        if(Edit1->Text != "") division = StrToInt(Edit1->Text);

        if(division) division = singles.single(division + FVP->GetRNValue());
        return(division);
}

//<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// set the division of the race
void TFDvlV::SetDivision(int division)
{
        ScrollBar1->Position = division;
}

//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
int TFDvlV::GetDivision(void)
{
        return(ScrollBar1->Position);
}
void __fastcall TFDvlV::FormClose(TObject *Sender, TCloseAction &Action)
{
        TIniFile *ini;

        ini = new TIniFile(ChangeFileExt(Application->ExeName,".INI"));
        ini->WriteInteger("DivForm","Top",Top);
        ini->WriteInteger("DivForm","Left",Left);

        delete ini;
}
//---------------------------------------------------------------------------

