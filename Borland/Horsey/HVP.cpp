//---------------------------------------------------------------------------
#include <vcl.h>
#include <inifiles.hpp>

#pragma hdrstop

#include "HVP.h"
#include "Alpha.h"
#include "SingleNum.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFVP *FVP;
//---------------------------------------------------------------------------
__fastcall TFVP::TFVP(TComponent* Owner)
        : TForm(Owner)
{
        TIniFile *ini;

        ini = new TIniFile(ChangeFileExt(Application->ExeName,".INI"));
        Top = ini->ReadInteger("VpForm","Top",10);
        Left = ini->ReadInteger("VpForm","Left",10);
        
        delete ini;
}
//---------------------------------------------------------------------------
void __fastcall TFVP::ERNChange(TObject *Sender)
{
        Edit1->Text = GetRNValue();
                        
}
//---------------------------------------------------------------------------
void __fastcall TFVP::Button1Click(TObject *Sender)
{
        TIniFile *ini;

        ini = new TIniFile(ChangeFileExt(Application->ExeName,".INI"));
        ini->WriteInteger("DivForm","Top",Top);
        ini->WriteInteger("DivForm","Left",Left);

        FVP->Hide();
        delete ini;
}
//---------------------------------------------------------------------------
// Returns the race name to the caller
AnsiString TFVP::GetRaceName(void)
{
        return(ERName->Text);
}

//---------------------------------------------------------------------------
// Returns the numerology value of the Race name
int TFVP::GetRNValue(void)
{
        int NameLen, Value, x;
        int NumAdd = 0;
        char first;
        AnsiString Temp;
        ALPHABET soup;
        SINGLE number;

        NameLen = ERName->Text.Length();

        NumAdd = soup.Get_First_Value(ERName->Text,1);

        for(x=2;x<=NameLen;x++)
        {
                first = soup.Get_Char(ERName->Text,x);
                Value = soup.Get_Egypt_Value(first);
                if(!Value)
                {
                        x++;
                        NumAdd += soup.Get_First_Value(ERName->Text,x);

                }
                NumAdd += Value;
                NumAdd = number.single(NumAdd);
        }

        return NumAdd;
}


//-----------------------------------------------------------------------------
// Return the Race name value in ansi string
AnsiString TFVP::AnsiRNValue(void)
{
        AnsiString temp;

        temp.printf("%d",GetRNValue());
        return(temp);
}

//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// Set the race name of the race
void TFVP::SetRaceName(char *racename)
{
        ERName->Text = racename;
}



//***********************************
//chage the new race name from string
void TFVP::ExRName(char *s)
{
        char *RetPtr;

        while(*s != ',') s++;
        s++;
        RetPtr=s;
        while(*s != ',') s++;

        *s=0;
        SetRaceName(RetPtr);
}
void __fastcall TFVP::FormClose(TObject *Sender, TCloseAction &Action)
{
        TIniFile *ini;

        ini = new TIniFile(ChangeFileExt(Application->ExeName,".INI"));
        ini->WriteInteger("VpForm","Top",Top);
        ini->WriteInteger("VpForm","Left",Left);

        delete ini;
}
//---------------------------------------------------------------------------

