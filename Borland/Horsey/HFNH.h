//---------------------------------------------------------------------------
#ifndef HFNHH
#define HFNHH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
//---------------------------------------------------------------------------
class TFFNH : public TForm
{
__published:	// IDE-managed Components
        TEdit *EFNH;
        TScrollBar *ScrollBar1;
        TLabel *Label1;
        TEdit *Edit1;
        TLabel *Label2;
        TLabel *Label3;
        TButton *Button1;
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall Scroll1(TObject *Sender);
        void __fastcall FNHChange(TObject *Sender);
        void __fastcall Button1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TFFNH(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFFNH *FFNH;
//---------------------------------------------------------------------------
#endif
