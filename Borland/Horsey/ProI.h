//---------------------------------------------------------------------------

#ifndef ProIH
#define ProIH
//---------------------------------------------------------------------------


class PROCESSI
{
private:
        int GetFirstPair(int value, int number);
        int GetSecPair1(int value, int number);
        int GetSecPair2(int value, int number);
        AnsiString GetRFigure(int value);
        bool IsUOWDV(int number, int value);
        bool IsUOWVH(int number, int value);
        bool IsOWDV(int number, int value);     // outright winner
        bool IsOWVH(int number, int value);     // outright winner


public:
        AnsiString AnsiProcess(int number, int value); // build a line 1 string
        AnsiString AnsiProcess(int number, int value, bool uf);
        int HowManyRF(int number,int value);
        AnsiString FindPreference(int number,int value);
        bool Exceptions(int number,int value);
        bool IsUOW(int number, int value);
        bool IsOW(int number, int value);
        bool IsUF(int number, int value);
        bool IsDF(int number,int value);        //double force
        bool IsFF(int number,int value);        //Full force
        bool IsSFF(int number, int value);	// special full force
        bool IsOrdFF(int number,int value);	// ordanary full force
};
#endif
