//---------------------------------------------------------------------------
#ifndef HDVLVH
#define HDVLVH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
//---------------------------------------------------------------------------
class TFDvlV : public TForm
{
__published:	// IDE-managed Components
        TEdit *Edit1;
        TEdit *Edit2;
        TScrollBar *ScrollBar1;
        TLabel *Label1;
        TLabel *Label2;
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall Ed1Exit(TObject *Sender);
        void __fastcall ScrollChange(TObject *Sender);
        void __fastcall Ed1Change(TObject *Sender);
        void __fastcall Button1Click(TObject *Sender);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
private:	// User declarations
public:		// User declarations
        __fastcall TFDvlV(TComponent* Owner);
        int GetDVLV(void);
        int GetDivision(void);
        void SetDivision(int division);
};
//---------------------------------------------------------------------------
extern PACKAGE TFDvlV *FDvlV;
//---------------------------------------------------------------------------
#endif
