//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "HDV.h"
#include "HDVLV.h"
#include "HVP.h"
#include "HNEnter.h"
#include "Alpha.h"

#include "UnitForce.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)



//---------------------------------------------------------------------------
// test for unit force with modification
// return the value if not unit force
// returns a ruling figure if unit force
int UNITFORCE::ModifyUForce(int number,int value)
{
        ALPHABET is;

        if(is.IsSame(number,value))
        {
                number = FDayValue->GetDaysValue();
                if(is.IsSame(number,value))
                {
                        number = FHorseNames->GetFNHValue();
                        if(is.IsSame(number,value))
                        {
                                number = FHorseNames->GetIDValue();
                                if(is.IsSame(number,value))
                                {
                                        number = FDvlV->GetDVLV();
                                        if((number == 0) || (is.IsSame(number,value)))
                                        {
                                                number = FVP->GetRNValue();
                                                if(is.IsSame(number,value))
                                                {
                                                        number = 0;
                                                }
                                        }
                                }
                        }
                }
        }
        return(number);
}

