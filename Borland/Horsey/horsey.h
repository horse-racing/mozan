//---------------------------------------------------------------------------
#ifndef horseyH
#define horseyH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <Menus.hpp>
#include <chartfx3.hpp>
#include <OleCtrls.hpp>
#include <ToolWin.hpp>
#include <Grids.hpp>
#include <Dialogs.hpp>


//---------------------------------------------------------------------------
class TMainForm1 : public TForm
{
__published:	// IDE-managed Components
        TStatusBar *StatusBar1;
        TMainMenu *MainMenu1;
        TMenuItem *File1;
        TMenuItem *Open1;
        TMenuItem *Save1;
        TMenuItem *Exit1;
        TMenuItem *Edit1;
        TMenuItem *Help1;
        TMenuItem *About1;
        TMenuItem *Help2;
        TButton *BDValue;
        TButton *BDvlV;
        TButton *BVP;
        TButton *BRefresh;
        TSaveDialog *SaveDialog1;
        TOpenDialog *OpenDialog1;
        TMenuItem *Option1;
        TMenuItem *Print1;
        TPrintDialog *PrintDialog1;
        TButton *BHorseNames;
        TRichEdit *REResults;
        TTabControl *TabControl1;
        void __fastcall BDValueClick(TObject *Sender);
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall BDvlVClick(TObject *Sender);
        void __fastcall BVPClick(TObject *Sender);
        void __fastcall BRefreshClick(TObject *Sender);
        void __fastcall Save1Click(TObject *Sender);
        void __fastcall Open1Click(TObject *Sender);
        void __fastcall Exit1Click(TObject *Sender);
        void __fastcall Print1Click(TObject *Sender);
        void __fastcall BHorseNamesClick(TObject *Sender);
        void __fastcall TabControl1Change(TObject *Sender);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);


private:	// User declarations
        AnsiString RaceNumber;
        AnsiString ExRace(AnsiString s);
        AnsiString ExNumber(AnsiString s);

public:		// User declarations
        __fastcall TMainForm1(TComponent* Owner);
        void ReadFile(AnsiString FileName);
        void ReadComaFile(AnsiString FileName);
        void SaveFile(AnsiString FileName);        
};
//---------------------------------------------------------------------------
extern PACKAGE TMainForm1 *MainForm1;
//---------------------------------------------------------------------------
#endif
