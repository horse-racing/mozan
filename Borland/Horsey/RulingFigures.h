//---------------------------------------------------------------------------

#ifndef RulingFiguresH
#define RulingFiguresH

#include "SingleNum.h"

//---------------------------------------------------------------------------

class RF : SINGLE
{
        private:


        public:
        RF(void);
        int DaysValue(int day,int month);
        int FullNumHorse(int horse);
        ~RF(void);
        
};

#endif
