//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Alpha.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

int ChaldeanOrder[10][10]=
{
        {0,0,0,0,0,0,0,0,0,0},
        {0,1,6,5,7,8,3,9,1,6},
        {0,2,8,3,9,1,6,5,7,8},
        {0,3,9,1,6,5,2,8,3,9},
        {0,4,6,5,7,8,3,9,1,6},
        {0,5,2,8,3,9,1,6,5,2},
        {0,6,5,2,8,3,9,1,6,5},
        {0,7,8,3,9,1,6,5,2,8},
        {0,8,3,9,1,6,5,2,8,3},
        {0,9,1,6,5,2,8,3,9,1},
};



int WeekDayOrder[10][10]=
{
        {0,0,0,0,0,0,0,0,0,0},
        {0,1,2,9,5,3,6,8,1,2},
        {0,2,9,5,3,6,8,1,2,9},
        {0,3,6,8,1,2,9,5,3,6},
        {0,4,2,9,5,3,6,8,1,2},
        {0,5,3,6,8,1,2,9,5,3},
        {0,6,8,1,2,9,5,3,6,8},
        {0,7,9,5,3,6,8,1,2,9},
        {0,8,1,2,9,5,3,6,8,1},
        {0,9,5,3,6,8,1,2,9,5},
};


//------------------------------------------------------------------
//
int ALPHABET::GetWDO(int points, int number)
{
        return(WeekDayOrder[number][points]);
}

//------------------------------------------------------------------
int ALPHABET::GetCLO(int points, int number)
{
        return(ChaldeanOrder[number][points]);
}

//---------------------------------------------------------
// Get Number of Points Chaldean order
//
int ALPHABET::GetNPointsI(int value, int number)
{
        int x,a;
        ALPHABET is;

        for(x=1;x<10;x++)
        {
                a=ChaldeanOrder[value][x];
                if(is.IsSame(a,number))
                {
                        return x;
                }
        }
        return 0;

}
//---------------------------------------------------------
// Get Number of Points Chaldean order
//
int ALPHABET::GetNPointsII(int value, int number)
{
        int x,a;
        ALPHABET is;

        for(x=1;x<10;x++)
        {
                a=WeekDayOrder[value][x];
                if(is.IsSame(a,number))
                {
                        return x;
                }
        }
        return 0;

}

//-----------------------------------------------------------------------------
// test for unit force
bool ALPHABET::IsSame(int number,int value)
{
        bool it = false;

        if(number == value) it = true;
        if((number == 1) && (value == 4)) it = true;
        if((number == 4) && (value == 1)) it = true;

        if((number == 2) && (value == 7)) it = true;
        if((number == 7) && (value == 2)) it = true;

        return it;
}

//---------------------------------------------------------
// Description: Get char from Ansistring
//
char ALPHABET::Get_Char(AnsiString Name, int Start)
{
        AnsiString Temp;
        char *ch;

        Temp = Name.SubString(Start,1);
        ch = Temp.c_str();
        return ch[0];
}

//--------------------------------------------------------
// Get letter Value
//
int ALPHABET::Get_Egypt_Value(char Letter)
{
        switch (Letter)
        {
                case 'a': case 'A':
                case 'i': case 'I':
                case 'y': case 'Y':
                        return 1;
                case 'b': case 'B':
                case 'k': case 'K':
                        return 2;
                case 'c': case 'C':
                case 'g': case 'G':
                case 'j': case 'J':
                case 'l': case 'L':
                case 'q': case 'Q':
                        return 3;
                case 'd': case 'D':
                case 'm': case 'M':
                case 'r': case 'R':
                case 't': case 'T':
                        return 4;
                case 'e': case 'E':
                case 'n': case 'N':
                        return 5;
                case 's': case 'S':
                case 'u': case 'U':
                case 'v': case 'V':
                case 'w': case 'W':
                        return 6;
                case 'x': case 'X':
                case 'z': case 'Z':
                        return 7;
                case 'f': case 'F':
                case 'h': case 'H':
                case 'o': case 'O':
                case 'p': case 'P':
                        return 8;
                default:
                        return 0;
        }
}

//>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<
// Description: get the first letter
//
int ALPHABET::Get_First_Value(AnsiString str, int index)
{
        AnsiString sub;
        char first, second;
        int Value;

        Value=0;
        first = Get_Char(str,index);

        switch(first)
        {
                case 'c':
                case 'C':
                case 's':
                case 'S':
                        second = Get_Char(str,index+1);
                        if(IsVowel(second))
                        {
                                Value +=Get_Hebrew_Value(first);
                        }
                        else
                        {
                                Value +=Get_Egypt_Value(first);
                        }
                        return Value;

                case 'o':
                case 'O':
                case 'r':
                case 'R':
                        Value +=Get_Hebrew_Value(first);
                        return Value;
                default:
                        Value +=Get_Egypt_Value(first);
                        return Value;
        }

}

//--------------------------------------------------------
// get the hebrew values
//
int ALPHABET::Get_Hebrew_Value(char Letter)
{
        switch (Letter)
        {
                case 'c':
                case 'C':
                case 'r':
                case 'R':
                        return 2;
                case 'o':
                case 'O':
                        return 7;
                case 's':
                case 'S':
                        return 3;
                default:
                        return 0;
        }
}


//----------------------------------------------------------
// Is char a Vowel
//
bool ALPHABET::IsVowel(char Letter)
{
        switch(Letter)
        {
                case 'a':
                case 'A':
                case 'e':
                case 'E':
                case 'i':
                case 'I':
                case 'o':
                case 'O':
                case 'u':
                case 'U':
                case 'y':
                case 'Y':
                        return true;
                default:
                        return false;
        }
}