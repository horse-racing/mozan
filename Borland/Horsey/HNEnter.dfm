object FHorseNames: TFHorseNames
  Left = 293
  Top = 606
  Width = 627
  Height = 469
  Caption = 'Enter the Horse Name'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 128
    Width = 51
    Height = 13
    Caption = 'History List'
  end
  object Ehorse: TEdit
    Left = 16
    Top = 48
    Width = 241
    Height = 21
    TabOrder = 0
    OnChange = EhorseChange
    OnKeyPress = Edit1KPress
  end
  object ListHorseHis: TListBox
    Left = 16
    Top = 144
    Width = 233
    Height = 201
    ItemHeight = 13
    Sorted = True
    TabOrder = 1
  end
  object Ehvalue: TEdit
    Left = 280
    Top = 48
    Width = 33
    Height = 21
    TabOrder = 2
  end
  object CheckBox1: TCheckBox
    Left = 16
    Top = 16
    Width = 73
    Height = 25
    Caption = 'Capitals'
    TabOrder = 3
    OnClick = CheckBox1Click
  end
  object BHisToHor: TButton
    Left = 272
    Top = 176
    Width = 65
    Height = 33
    Caption = '--->'
    TabOrder = 4
    OnClick = BHisToHorClick
  end
  object Button2: TButton
    Left = 16
    Top = 352
    Width = 49
    Height = 25
    Caption = 'Delete'
    TabOrder = 5
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 192
    Top = 352
    Width = 57
    Height = 25
    Caption = 'Save'
    TabOrder = 6
    OnClick = Button3Click
  end
  object BTransfer: TButton
    Left = 536
    Top = 352
    Width = 65
    Height = 25
    Caption = 'OKIDOKI'
    TabOrder = 7
    OnClick = BTransferClick
  end
  object ListRHorse: TListBox
    Left = 360
    Top = 144
    Width = 241
    Height = 201
    ItemHeight = 13
    TabOrder = 8
    OnClick = ListRHorseClick
  end
  object Edit3: TEdit
    Left = 456
    Top = 352
    Width = 41
    Height = 21
    TabOrder = 9
  end
  object BListDel: TButton
    Left = 360
    Top = 352
    Width = 57
    Height = 25
    Caption = 'Delete'
    TabOrder = 10
    OnClick = BListDelClick
  end
  object Button5: TButton
    Left = 368
    Top = 112
    Width = 65
    Height = 25
    Caption = 'Edit Horse'
    TabOrder = 11
    OnClick = Button5Click
  end
  object BMup: TButton
    Left = 448
    Top = 112
    Width = 73
    Height = 25
    Caption = 'Move Up'
    TabOrder = 12
    OnClick = BMupClick
  end
  object BMdwn: TButton
    Left = 528
    Top = 112
    Width = 73
    Height = 25
    Caption = 'Move Down'
    TabOrder = 13
    OnClick = BMdwnClick
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 392
    Width = 619
    Height = 23
    Panels = <>
    SimplePanel = False
  end
  object Button1: TButton
    Left = 528
    Top = 80
    Width = 73
    Height = 25
    Caption = 'Delete All'
    TabOrder = 15
    OnClick = Button1Click
  end
  object MainMenu1: TMainMenu
    Left = 592
    object F1: TMenuItem
      Caption = '&File'
    end
  end
  object OpenDialog1: TOpenDialog
    DefaultExt = 'hdb'
    FileName = 'Horse.hdb'
    Filter = 'Horse DataBase "*.hdb"|hdb'
    Title = 'Open Horse Data Base'
    Left = 560
  end
end
