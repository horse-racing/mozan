//---------------------------------------------------------------------------
#ifndef HVPH
#define HVPH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
//---------------------------------------------------------------------------
class TFVP : public TForm
{
__published:	// IDE-managed Components
        TEdit *ERName;
        TEdit *Edit1;
        TButton *Button1;
        void __fastcall ERNChange(TObject *Sender);
        void __fastcall Button1Click(TObject *Sender);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
private:	// User declarations
public:		// User declarations
        __fastcall TFVP(TComponent* Owner);
        AnsiString GetRaceName(void);
        int GetRNValue(void);
        AnsiString AnsiRNValue(void);
        void SetRaceName(char *racename);
        void ExRName(char *s);
};
//---------------------------------------------------------------------------
extern PACKAGE TFVP *FVP;
//---------------------------------------------------------------------------
#endif
