//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "SingleNum.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)
//--------------------------------------------------------
// Calculate Single Digit Numerology Number
// all the routines use this
//----------------------------------------------------------
int SINGLE::single(int Number)
{
        int a,b;

        while(Number >= 10) //loop if numer is 10 or greater
        {
                a=Number/10;    // divide nuber by 10 to get upper number
                b=Number%10;    // mod number to get the lower number
                Number=a+b;     // add them together to get new number
        }
        return(Number);         // finished return new number
}
