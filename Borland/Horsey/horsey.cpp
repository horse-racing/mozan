//---------------------------------------------------------------------------
#include <vcl.h>
#include <stdio.h>
#include <dir.h>
#include <printers.hpp>
#include <winver.h>
#include <inifiles.hpp>

#pragma hdrstop

#include "horsey.h"

#include "HDV.h"
#include "HDVLV.h"
#include "HVP.h"
#include "HNEnter.h"
#include "version.h"


//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"

#define ProTitle "Mozan Racing Numerology   "




TMainForm1 *MainForm1;

//---------------------------------------------------------------------------
__fastcall TMainForm1::TMainForm1(TComponent* Owner)
        : TForm(Owner)
{
        TIniFile *ini;

        ini = new TIniFile(ChangeFileExt(Application->ExeName,".INI"));
        Top = ini->ReadInteger("MainForm","Top",10);
        Left = ini->ReadInteger("MainForm","Left",10);
        
        delete ini;
}
//---------------------------------------------------------------------------




void __fastcall TMainForm1::FormCreate(TObject *Sender)
{
        VERSION ver;
        AnsiString Version;

        Version= ver.exever();
        Caption.sprintf("Mozan Racing Numerology Version ") + Version;

        BDValue->Hint = "Enter the date the race is on";
        BDvlV->Hint = "Enter the Division of the Race";
        BVP->Hint = "Enter the Name of Race";
        BRefresh->Hint = "Refresh the Results";
}

//---------------------------------------------------------------------------
void __fastcall TMainForm1::BDValueClick(TObject *Sender)
{
        FDayValue->Show();
}
//---------------------------------------------------------------------------



//---------------------------------------------------------------------------

void __fastcall TMainForm1::BDvlVClick(TObject *Sender)
{
        FDvlV->Show();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm1::BVPClick(TObject *Sender)
{
        FVP->Show();

}
//---------------------------------------------------------------------------

void __fastcall TMainForm1::BRefreshClick(TObject *Sender)
{
//        VERSION ver;
        AnsiString version;
        int nhorses = 0;
        int x;
        int horsen;
        bool show = false;

        // Form title and version of program
        version= Version.GetVer();
        Caption = "Mozan Racing Numerology Version " + version;
        REResults->DefAttributes->Color = clBlack;
        REResults->Lines->Clear();
        REResults->Lines->Add(Caption);
        REResults->Lines->Add(RaceNumber);
        version.printf("%s    %s",FDayValue->GetDateStr(),FVP->GetRaceName());
        REResults->Lines->Add(version);
        REResults->Lines->Add("");
        version.printf("A. Day's Value: %d",FDayValue->GetDaysValue());
        REResults->Lines->Add(version);
        version.printf("B. Full Number of Horses: %d",FHorseNames->GetFNHValue());
        REResults->Lines->Add(version);
        version.printf("a. Initials' Digit: %d",FHorseNames->GetIDValue());
        REResults->Lines->Add(version);
        version.printf("b. Divisional Value: %d",FDvlV->GetDVLV());
        REResults->Lines->Add(version);
        version.printf("c. Value of the Plate: %d",FVP->GetRNValue());
        REResults->Lines->Add(version);
        REResults->Lines->Add("");

        nhorses = FHorseNames->Count();         //get the number of horses in race

        for(x=0,horsen=1; x<nhorses; x++,horsen++)
        {
                switch(TabControl1->TabIndex)
                {
                        case 0: // show all
                                show = true;
                                break;
                        case 1: // Show only Unit Force Outwright Winner
                                if(FHorseNames->Pro1IsUOW(x)) show = true;
                                if(FHorseNames->Pro2IsUOW(x)) show = true;
                                break;

                        case 2: // Show only Outwright Winner
                                if(FHorseNames->Pro1IsOW(x)) show = true;
                                if(FHorseNames->Pro2IsOW(x)) show = true;                                
                                break;
                        case 3: // Show only Unit Force
                                if(FHorseNames->Pro1IsUF(x)) show = true;
                                if(FHorseNames->Pro2IsUF(x)) show = true;
                                break;
                        case 4: // Show Only Double Force
                                if(FHorseNames->Pro1IsDF(x)) show = true;
                                if(FHorseNames->Pro2IsDF(x)) show = true;
                                break;
                        case 5: // Show only Full Force
                                if(FHorseNames->Pro1IsFF(x)) show = true;
                                if(FHorseNames->Pro2IsFF(x)) show = true;
                                break;
                        case 6: // show only Exceptions
                                if(FHorseNames->IsException(x)) show = true;
                                break;
                }
                if(show)
                {
                        show = false;
                        version.printf("%d %s %d",horsen,FHorseNames->HorseName(x),FHorseNames->HorseValue(x));
                        REResults->Lines->Add(version);
                        version.printf("Process I:   %s",FHorseNames->ProIFPair(x));
                        REResults->Lines->Add(version);
                        version.printf("Process II:  %s",FHorseNames->ProIIFPair(x));
                        REResults->Lines->Add(version);
                        version.printf("Total Ruling Figures:  %d",FHorseNames->TotalRF(x));
                        REResults->Lines->Add(version);
                        version.printf("Direct Connection:  %s",FHorseNames->FindDC(x));
                        REResults->Lines->Add(version);
                        version.printf("Process I Preference Order:  %s",FHorseNames->PrefProI(x));
                        REResults->Lines->Add(version);
                        version.printf("Process II Preference Order:  %s",FHorseNames->PrefProII(x));
                        REResults->Lines->Add(version);
                        version.printf("Exceptions:   %s",FHorseNames->Exceptions(x));
                        REResults->Lines->Add(version);
                        REResults->Lines->Add("----------------------------------------------");
                }
        }
}
//---------------------------------------------------------------------------



void __fastcall TMainForm1::Save1Click(TObject *Sender)
{
        AnsiString Temp;

	if (SaveDialog1->Execute())
	{
                SaveFile(SaveDialog1->FileName);
	}
}

//---------------------------------------------------------------------------

void __fastcall TMainForm1::Open1Click(TObject *Sender)
{
        if (OpenDialog1->Execute())
        {
                switch(OpenDialog1->FilterIndex)
                {
                        case 1: // open standard horse file
                                ReadFile(OpenDialog1->FileName);
                                break;
                        case 2: // Coma delimited file fro TAB
                                ReadComaFile(OpenDialog1->FileName);
                                break;
                }
        }
        TabControl1->TabIndex = 0;
        BRefreshClick(Sender);
}

//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>
// Extract the year from string
//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>
AnsiString TMainForm1::ExRace(AnsiString s)
{
        AnsiString asg;
        int x;


        x=s.Pos(",");
        if(x != 0)
        {
                x++;
                s = s.SubString(x,256);
                x=s.Pos(",");
                if(x > 1) asg = s.SubString(1,--x);
        }
        return(asg);
}

//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>
// Extract the year from string
//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>
AnsiString TMainForm1::ExNumber(AnsiString s)
{
        AnsiString asg;
        int x;


        x=s.Pos(",");
        if(x > 1) asg = s.SubString(1,--x);

        return(asg);
}
//******************************************************************
// Read Coma Delimited file from TAB
void TMainForm1::ReadComaFile(AnsiString FileName)
{
	int iFileHandle;
        int iBytesRead;
        int iLength;
        int x;
        char Buff[256];
        FILE *stream;
        char *s;


	if(FileExists(FileName))
	{
                stream = fopen(FileName.c_str(),"rt");
                if(stream != NULL)
                {
                        s=fgets(Buff,256,stream); //first line
                        s=fgets(Buff,256,stream);// second line
                        s=fgets(Buff,256,stream);// Third line
                        s=fgets(Buff,256,stream);// fourth line
                        s=fgets(Buff,256,stream);// fith line
                        FDayValue->SetDate(FDayValue->ExDate(s));
                        FDayValue->SetMonth(FDayValue->ExMonth(s));
                        FDayValue->SetYear(FDayValue->ExYear(s));
                        RaceNumber = ExRace(s);
                        s=fgets(Buff,256,stream);// sixth line
                        s=fgets(Buff,256,stream);// seventh line
                        s=fgets(Buff,256,stream);// eigth line
                        s=fgets(Buff,256,stream);// nine line
                        RaceNumber = RaceNumber + ExNumber(s);
                        FVP->ExRName(s);

                        s=fgets(Buff,256,stream);// ten line
                        s=fgets(Buff,256,stream);// eleven line
                        s=fgets(Buff,256,stream);// twelve line
                        s=fgets(Buff,256,stream);// thirteen line
                        x=0;
                        FHorseNames->ClearHorse();
                        while(*s != '\n')
                        {
                                x++;
                                FHorseNames->ExHName(s);
                                s=fgets(Buff,256,stream);// thirteen line
                        }
                        FDvlV->SetDivision(0);
                }
                fclose(stream);
        }
}


//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// read the saved files
//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>
void TMainForm1::ReadFile(AnsiString FileName)
{
	int iFileHandle;
        int iBytesRead;
        int iLength;
        int x;
        int lasthorse;
        char Buff[256];

	if(FileExists(FileName))
	{
                // Get Version
                iFileHandle = FileOpen(FileName, fmOpenRead);
                iBytesRead = FileRead(iFileHandle, (char *)&iLength, sizeof(iLength));
                iBytesRead = FileRead(iFileHandle,Buff, iLength);
                Buff[iBytesRead]=0;
//                RaceData.Version=Buff;

                // Get Day
                iBytesRead = FileRead(iFileHandle,(char *)&iLength, sizeof(iLength));
                FDayValue->SetDate((Word)iLength);

                // Get Month
                iBytesRead = FileRead(iFileHandle, (char *)&iLength, sizeof(iLength));
                FDayValue->SetMonth((Word)iLength);

                // Get Year
                iBytesRead = FileRead(iFileHandle, (char *)&iLength, sizeof(iLength));
                FDayValue->SetYear((Word)iLength);

                // Get Last Horse
                iBytesRead = FileRead(iFileHandle, (char *)&iLength, sizeof(iLength));
                lasthorse=iLength;

                // Get the Division
                iBytesRead = FileRead(iFileHandle, (char *)&iLength, sizeof(iLength));
                FDvlV->SetDivision(iLength);

                // need to clear previous horses
                FHorseNames->ClearHorse();
                // Get the Horse names
                for(x=0; x < lasthorse;x++)
                {
                        iBytesRead = FileRead(iFileHandle, (char *)&iLength, sizeof(iLength));
                        iBytesRead = FileRead(iFileHandle,Buff, iLength);
                        Buff[iBytesRead]=0;
                        FHorseNames->AddHorse(Buff);
                }

                // Get Race Name
                iBytesRead = FileRead(iFileHandle, (char *)&iLength, sizeof(iLength));
                iBytesRead = iBytesRead;
                iBytesRead = FileRead(iFileHandle,Buff, iLength);
                Buff[iBytesRead]=0;
                FVP->SetRaceName(Buff);

                FileClose(iFileHandle);
        }

}

//----------------------------------------------------------
// Save file Name
//
void TMainForm1::SaveFile(AnsiString FileName)
{
        VERSION ver;
	char szFileName[MAXFILE+4];
	int iFileHandle;
	int iLength;
        AnsiString Temp;
        int x;

	if (FileExists(FileName))
	{
                fnsplit(FileName.c_str(), 0, 0, szFileName, 0);
                strcat(szFileName, ".BAK");
                RenameFile(FileName, szFileName);
        }
        iFileHandle = FileCreate(FileName);

        // Write out the version
        Temp = ver.exever();
        iLength=Temp.Length();
        FileWrite(iFileHandle,(char *)&iLength, sizeof(iLength));
	FileWrite(iFileHandle,Temp.c_str(),Temp.Length());

        // Write out the Day
        iLength = FDayValue->GetDate();
        FileWrite(iFileHandle,(char *)&iLength, sizeof(iLength));

        // Write out the Month
        iLength = FDayValue->GetMonth();
        FileWrite(iFileHandle,(char *)&iLength, sizeof(iLength));

        // Write out the Year
        iLength = FDayValue->GetYear();
        FileWrite(iFileHandle,(char *)&iLength, sizeof(iLength));

        // Write out the Last horse
        iLength = FHorseNames->Count();
        FileWrite(iFileHandle,(char *)&iLength, sizeof(iLength));

        // Write out the division
        iLength = FDvlV->GetDivision();
        FileWrite(iFileHandle,(char *)&iLength, sizeof(iLength));

        // Write out the horse names
        for(x=0; x <FHorseNames->Count();x++)
        {
                Temp = FHorseNames->HorseName(x);
                iLength=Temp.Length();
                FileWrite(iFileHandle,(char *)&iLength, sizeof(iLength));
                FileWrite(iFileHandle,Temp.c_str(),Temp.Length());
        }

        // Write out the Race Name
        Temp = FVP->GetRaceName();
        iLength=Temp.Length();
        FileWrite(iFileHandle,(char *)&iLength, sizeof(iLength));
        FileWrite(iFileHandle,Temp.c_str(),Temp.Length());


	FileClose(iFileHandle);

}


void __fastcall TMainForm1::Exit1Click(TObject *Sender)
{
        MainForm1->Close();
}
//---------------------------------------------------------------------------
void __fastcall TMainForm1::Print1Click(TObject *Sender)
{
        if(PrintDialog1->Execute())
        {
                REResults->Print("RACING NUMEROLOGY");
        }
}
//---------------------------------------------------------------------------



void __fastcall TMainForm1::BHorseNamesClick(TObject *Sender)
{
        FHorseNames->Show();
//        BRefreshClick(Sender);
}
//---------------------------------------------------------------------------


void __fastcall TMainForm1::TabControl1Change(TObject *Sender)
{
        BRefreshClick(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm1::FormClose(TObject *Sender,
      TCloseAction &Action)
{
        TIniFile *ini;

        ini = new TIniFile(ChangeFileExt(Application->ExeName,".INI"));
        ini->WriteInteger("MainForm","Top",Top);
        ini->WriteInteger("MainForm","Left",Left);

        delete ini;
}
//---------------------------------------------------------------------------

