//---------------------------------------------------------------------------

#ifndef VersionH
#define VersionH
//---------------------------------------------------------------------------


class VERSION
{
        private:
        AnsiString version;     // saved version string
        char *pValue;
        char *pBuf;
        
        public:
        VERSION(void);  // Exract the version
        AnsiString exever(void){return(version);}    // get the exe version
        AnsiString GetVer(void);
};


extern VERSION Version;

#endif
