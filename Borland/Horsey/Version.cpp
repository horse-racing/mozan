//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Version.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)
#define VERMAJOR        1
#define VERMINOR        0
#define VERRELEASE      2
#define VERBUILD        37

VERSION Version;

//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>
// Description: Version constructor
// Arguments:
// Returns:
// Created:     22/9/2003
// Notes:
// <<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
VERSION::VERSION(void)
{
        unsigned long lSize,Size;
        unsigned int iLen;
        char *pBuf;
        LPTSTR sFile = Application->ExeName.c_str();

        Size=0;
        lSize = GetFileVersionInfoSize(sFile,&Size);
        if(lSize>0)
        {
                pValue = new char[1024];
                pBuf = new char[lSize];
                GetFileVersionInfo(sFile,0,lSize,pBuf);
                if(VerQueryValue(pBuf,"StringFileInfo\\040904E4\\FileVersion",(void**) &pValue,&iLen))
                {
                        version = StrPas(pValue);
                }
                delete pValue;
                delete pBuf;
        }
        else version.sprintf("%d.%d.%d.%d",VERMAJOR,VERMINOR,VERRELEASE,VERBUILD);
}



//((((((((((((((((((((((((((((((((((()))))))))))))))))))))))))))))
//get the version of this executable
//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
AnsiString VERSION::GetVer(void)
{

        return(version);
}
