//---------------------------------------------------------------------------

#ifndef SingleNumH
#define SingleNumH
//---------------------------------------------------------------------------
// header file to handle the addition of numbers to single number
//

class SINGLE
{
        public:
        int single(int number);         // returns a single number

};
#endif
