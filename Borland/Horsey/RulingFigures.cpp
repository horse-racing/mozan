//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "RulingFigures.h"

//---------------------------------------------------------------------------
// Ruling figure class is self constructed in variable rulingfigure variable
// and is global throught all installation of header file
//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
#pragma package(smart_init)


//--------------------------------------------------------------------------
// Days Value data
int DVData[13][32]=
{
        {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
        {0,6,6,6,6,6,6,6,6,6,5,5,5,5,5,5,2,5,5,4,4,4,4,4,4,4,4,4,3,3,3,3}, //jan
        {0,6,6,6,6,6,6,6,5,5,5,5,5,5,5,5,2,4,4,4,4,4,4,4,4,4,3,3,3,3,0,0}, //feb
        {0,6,6,6,6,6,5,5,5,5,5,5,5,5,5,4,2,4,4,4,4,4,4,4,3,3,3,3,3,3,3,3}, //march
        {0,6,6,6,5,5,5,5,5,5,5,5,5,4,4,4,2,4,4,4,4,4,3,3,3,3,3,3,3,3,3,0}, //april
        {0,6,5,5,5,5,5,5,5,5,5,4,4,4,4,4,1,4,4,4,3,3,3,3,3,3,3,3,3,2,2,2}, //may
        {0,5,5,5,5,5,5,5,5,4,4,4,4,4,4,4,1,4,3,3,3,3,3,3,3,3,3,2,2,2,2,0}, //june
        {0,5,5,5,5,5,5,4,4,4,4,4,4,4,4,4,1,3,3,3,3,3,3,3,3,2,2,2,2,2,2,2}, //july
        {0,5,5,5,5,4,4,4,4,4,4,4,4,4,3,3,1,3,3,3,3,3,3,2,2,2,2,2,2,2,2,2}, //august
        {0,5,5,4,4,4,4,4,4,4,4,4,3,3,3,3,1,3,3,3,3,2,2,2,2,2,2,2,2,2,1,0}, //sept
        {0,4,4,4,4,4,4,4,4,4,3,3,3,3,3,3,7,3,3,2,2,2,2,2,2,2,2,2,1,1,1,1}, //oct
        {0,4,4,4,4,4,4,4,3,3,3,3,3,3,3,3,9,2,2,2,2,2,2,2,2,2,1,1,1,1,1,0}, //nov
        {0,4,4,4,4,4,3,3,3,3,3,3,3,3,3,2,9,2,2,2,2,2,2,2,1,1,1,1,1,1,1,1}, //dec
};

//<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//Description: The constructor for the ruling figures for the race
//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
RF::RF(void)
{


}


//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//Description:  The Days Value
//Arguments:    Day
//              Month
//Returns:      Numerology days value
//Notes:        Uses a lookup table of day values
//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
int RF::DaysValue(int day, int month)
{
        int retval = 0;

        if(day < 32)    // make sure that the day is with in limits
        {
                if(month < 13)  // make sure month is with in limits
                {
                        retval = DVData[month][day];
                }
        }
        return(retval);
}


//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//Description:  The Full number of the horse
//Arguments:    The number of the last horse in the race
//Returns:      The Full Number of the Horse (F.N.H.)
//Notes:        This routine makes sure the a single digit number is returned
//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
int RF::FullNumHorse(int horse)
{
        return(single(horse));

}

//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//Description:  The destructor for the ruling figures for the race
//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
RF::~RF(void)
{


}
