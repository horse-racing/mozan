//---------------------------------------------------------------------------

#ifndef HorseH
#define HorseH

#include "Alpha.h"
#include "SingleNum.h"

//---------------------------------------------------------------------------

class HORSENAME: ALPHABET, SINGLE
{
        private:
        AnsiString name;
        int number;
        TStringList *horselist;

        public:
        int Get_Value(void);
        HORSENAME(void);        // constructor
        ~HORSENAME(void);       // destructor

        void Set_Name(AnsiString horsename) {name = horsename;}
        void Set_Name(AnsiString horsename, int horsenumber);
};

extern HORSENAME horse[30];
#endif
