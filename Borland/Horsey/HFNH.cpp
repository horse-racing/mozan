//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "HFNH.h"
#include "Horsey2.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFFNH *FFNH;
//---------------------------------------------------------------------------
__fastcall TFFNH::TFFNH(TComponent* Owner)
        : TForm(Owner)
{
        RaceInfo.SetLH(8);    // set the last horse to 8
}
//---------------------------------------------------------------------------
void __fastcall TFFNH::FormCreate(TObject *Sender)
{
        FFNH->Caption = "Full Number Of Horses";
        if(RaceInfo.GetLH() < 8)
        {
                RaceInfo.SetLH(ScrollBar1->Position);

        }
        else
        {
                ScrollBar1->Position=RaceInfo.GetLH();
        }
        Scroll1(Sender);
                       
}
//---------------------------------------------------------------------------
void __fastcall TFFNH::Scroll1(TObject *Sender)
{

        EFNH->Text = IntToStr(ScrollBar1->Position);
        RaceInfo.SetLH(ScrollBar1->Position);
        Edit1->Text = IntToStr(RaceInfo.GetFNH());
                       
}
//---------------------------------------------------------------------------
void __fastcall TFFNH::FNHChange(TObject *Sender)
{
        int a;

        if(EFNH->Text != "") a=StrToInt(EFNH->Text);

        if(a > 30)
        {
                a=30;
        }
        if(a < 8)
        {
                a=8;
        }

        EFNH->Text=IntToStr(a);
        
}
//---------------------------------------------------------------------------

void __fastcall TFFNH::Button1Click(TObject *Sender)
{

        RaceInfo.SetLH(ScrollBar1->Position);
        FFNH->Hide();

}
//---------------------------------------------------------------------------

