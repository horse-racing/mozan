//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "HDV.h"
#include "HDVLV.h"
#include "HVP.h"
#include "HNEnter.h"
#include "Alpha.h"
#include "Serialsel.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

//**********************************************************
// Get Number test if Ruling Figure return letter representation
//**********************************************************
AnsiString SERIALSELECT::GetRFigurex(int value)
{
        ALPHABET is;
        AnsiString letter;

        if(is.IsSame(value,FDayValue->GetDaysValue())) letter = "A";
        if(is.IsSame(value,FHorseNames->GetFNHValue())) letter = letter + "B";
        if(is.IsSame(value,FHorseNames->GetIDValue())) letter = letter + "a";
        if(is.IsSame(value,FDvlV->GetDVLV())) letter = letter + "b";
        if(is.IsSame(value,FVP->GetRNValue())) letter = letter + "c";
        if(letter.IsEmpty()) letter = "X";
        return(letter);
}

bool SERIALSELECT::IsSameRF(int number)
{
        ALPHABET is;
        bool temp = false;

        if(is.IsSame(number,FDayValue->GetDaysValue())) temp = true;
        if(is.IsSame(number,FHorseNames->GetFNHValue())) temp = true;
        if(is.IsSame(number,FHorseNames->GetIDValue())) temp = true;
        if(is.IsSame(number,FDvlV->GetDVLV())) temp = true;
        if(is.IsSame(number,FVP->GetRNValue())) temp = true;

        return(temp);
}

//--------------------------------------------------------------------
AnsiString SERIALSELECT::SerialSel(int value, int number, int profpair)
{
        AnsiString temp;


        if(IsSameRF(number))
        {
                if(IsSameRF(profpair))
                {
                        temp="1" + GetRFigurex(profpair) + "(" + GetRFigurex(value) + ")";
                }
                else
                {
                        temp="3" + GetRFigurex(number) + "(" + GetRFigurex(value) + ")";
                }
        }
        else
        {
                if(IsSameRF(profpair))
                {
                        temp="2" + GetRFigurex(profpair) + "(" + GetRFigurex(value) + ")";
                }
                else
                {
                        temp="4X(" + GetRFigurex(value) + ")";
                }
        }

        return(temp);
}


//--------------------------------------------------------------------
AnsiString SERIALSELECT::SerialSel(int value, int number, int profpair, bool uf)
{
        AnsiString temp;
        AnsiString uft;

        if(uf) uft = "U";
        else uft = "";

        if(IsSameRF(number))
        {
                if(IsSameRF(profpair))
                {
                        temp= uft + "1" + GetRFigurex(profpair) + "(" + GetRFigurex(value) + ")";
                }
                else
                {
                        temp= uft + "3" + GetRFigurex(number) + "(" + GetRFigurex(value) + ")";
                }
        }
        else
        {
                if(IsSameRF(profpair))
                {
                        temp= uft + "2" + GetRFigurex(profpair) + "(" + GetRFigurex(value) + ")";
                }
                else
                {
                        temp= uft + "4X(" + GetRFigurex(value) + ")";
                }
        }

        return(temp);
}


//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// count how many rf match the number
int SERIALSELECT::CountRF(int number)
{
        ALPHABET is;
        int count = 0;

        if(is.IsSame(number,FDayValue->GetDaysValue())) count++;
        if(is.IsSame(number,FHorseNames->GetFNHValue())) count++;
        if(is.IsSame(number,FHorseNames->GetIDValue())) count++;
        if(is.IsSame(number,FDvlV->GetDVLV())) count++;
        if(is.IsSame(number,FVP->GetRNValue())) count++;

        return(count);

}


//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// find ou the three number passed is the dv
bool SERIALSELECT::IsDV(int value,int number,int found)
{
        bool dvfound = false;
        bool fnhfound = false;
        bool idfound = false;
        bool vpfound = false;
        bool result = false;

        ALPHABET is;

        if(is.IsSame(value,FDayValue->GetDaysValue())) dvfound = true;
        if(is.IsSame(number,FDayValue->GetDaysValue())) dvfound = true;
        if(is.IsSame(found,FDayValue->GetDaysValue())) dvfound = true;

        if(is.IsSame(value,FHorseNames->GetFNHValue())) fnhfound = true;
        if(is.IsSame(number,FHorseNames->GetFNHValue())) fnhfound = true;
        if(is.IsSame(found,FHorseNames->GetFNHValue())) fnhfound = true;

        if(is.IsSame(value,FHorseNames->GetIDValue())) idfound = true;
        if(is.IsSame(number,FHorseNames->GetIDValue())) idfound = true;
        if(is.IsSame(found,FHorseNames->GetIDValue())) idfound = true;

        if(is.IsSame(value,FVP->GetRNValue())) vpfound = true;
        if(is.IsSame(number,FVP->GetRNValue())) vpfound = true;
        if(is.IsSame(found,FVP->GetRNValue())) vpfound = true;

        if(dvfound && fnhfound && idfound) result = true;
        if(dvfound && fnhfound && vpfound) result = true;

        return(result);
}


//-----------------------------------------------------------------------------
//Description:  Technicalities of the system
//Arguments:    value to be tested
//Returns:      True if tech
//              False if not
//Notes:        Need to read Chapter V of Racing Numerology
//>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
bool SERIALSELECT::Is_ChapV(int value)
{
        bool dc = false;
        SERIALSELECT serial;

        if((value == 8) && (serial.IsSameRF(3))) dc = true;
        if((value == 3) && (serial.IsSameRF(8))) dc = true;
        if((value == 2) && (serial.IsSameRF(9))) dc = true;
        if((value == 7) && (serial.IsSameRF(9))) dc = true;
        if(value == 9)
        {
                if(serial.IsSameRF(2)) dc = true;
                if(serial.IsSameRF(7)) dc = true;
        }


        return(dc);
}

//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//Description:  Test for Direct Connection
//Arguments:    number of the horse
//              value of the horse
//Returns:      0 for no connection
//              1 connection to value of the horse
//              2 connection to number of the horse
//              3 connection to number and value of horse
//Notes:        The Grounds on which this system is based, is that the horse and
//              or the value having the same or similar number to any of the ruling
//              figures.
//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
int SERIALSELECT::Is_DC(int number, int value)
{
        int retval = 0;
        SERIALSELECT st;

        if(st.IsSameRF(value)) retval |= 0x01;
        if(st.IsSameRF(number)) retval |= 0x02;
        if(Is_ChapV(value)) retval |= 0x01;
        if(Is_ChapV(number)) retval |= 0x02;

        return(retval);
}


//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// find ou the three number passed is the dv
bool SERIALSELECT::Is_DV(int value,int number,int found)
{
        bool result = false;

        ALPHABET is;

        if(is.IsSame(value,FDayValue->GetDaysValue())) result = true;
        if(is.IsSame(number,FDayValue->GetDaysValue())) result = true;
        if(is.IsSame(found,FDayValue->GetDaysValue())) result = true;

        return(result);
}

//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// find ou the three number passed is the dv
bool SERIALSELECT::Is_DV(int number,int found)
{
        bool result = false;

        ALPHABET is;

        if(is.IsSame(number,FDayValue->GetDaysValue())) result = true;
        if(is.IsSame(found,FDayValue->GetDaysValue())) result = true;

        return(result);
}


//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// find ou the three number passed is the dv
bool SERIALSELECT::Is_DV(int number)
{
        bool result = false;

        ALPHABET is;

        if(is.IsSame(number,FDayValue->GetDaysValue())) result = true;

        return(result);
}

//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// find ou the three number passed is the dv
bool SERIALSELECT::Is_FNH(int value,int number,int found)
{
        bool result = false;

        ALPHABET is;

        if(is.IsSame(value,FHorseNames->GetFNHValue())) result = true;
        if(is.IsSame(number,FHorseNames->GetFNHValue())) result = true;
        if(is.IsSame(found,FHorseNames->GetFNHValue())) result = true;

        return(result);
}


//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// find ou the three number passed is the fnh
bool SERIALSELECT::Is_FNH(int number,int found)
{
        bool result = false;

        ALPHABET is;

        if(is.IsSame(number,FHorseNames->GetFNHValue())) result = true;
        if(is.IsSame(found,FHorseNames->GetFNHValue())) result = true;

        return(result);
}

//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// find ou the three number passed is the fnh
bool SERIALSELECT::Is_FNH(int number)
{
        bool result = false;

        ALPHABET is;

        if(is.IsSame(number,FHorseNames->GetFNHValue())) result = true;

        return(result);
}

//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// find ou the three number passed is the dv
bool SERIALSELECT::Is_ID(int value,int number,int found)
{
        bool result = false;

        ALPHABET is;

        if(is.IsSame(value,FHorseNames->GetIDValue())) result = true;
        if(is.IsSame(number,FHorseNames->GetIDValue())) result = true;
        if(is.IsSame(found,FHorseNames->GetIDValue())) result = true;

        return(result);
}

//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// find ou the three number passed is the dv
bool SERIALSELECT::Is_ID(int number,int found)
{
        bool result = false;

        ALPHABET is;

        if(is.IsSame(number,FHorseNames->GetIDValue())) result = true;
        if(is.IsSame(found,FHorseNames->GetIDValue())) result = true;

        return(result);
}

//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// find ou the three number passed is the dv
bool SERIALSELECT::Is_ID(int number)
{
        bool result = false;

        ALPHABET is;

        if(is.IsSame(number,FHorseNames->GetIDValue())) result = true;

        return(result);
}



//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// find ou the three number passed is the dv
bool SERIALSELECT::Is_DVL(int value,int number,int found)
{
        bool result = false;

        ALPHABET is;

        if(is.IsSame(value,FDvlV->GetDVLV())) result = true;
        if(is.IsSame(number,FDvlV->GetDVLV())) result = true;
        if(is.IsSame(found,FDvlV->GetDVLV())) result = true;

        return(result);
}

//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// find ou the three number passed is the dv
bool SERIALSELECT::Is_DVL(int number,int found)
{
        bool result = false;

        ALPHABET is;

        if(is.IsSame(number,FDvlV->GetDVLV())) result = true;
        if(is.IsSame(found,FDvlV->GetDVLV())) result = true;

        return(result);
}

//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// find ou the three number passed is the dv
bool SERIALSELECT::Is_DVL(int number)
{
        bool result = false;

        ALPHABET is;

        if(is.IsSame(number,FDvlV->GetDVLV())) result = true;

        return(result);
}

//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// find ou the three number passed is the dv
bool SERIALSELECT::Is_VP(int value,int number,int found)
{
        bool result = false;

        ALPHABET is;

        if(is.IsSame(value,FVP->GetRNValue())) result = true;
        if(is.IsSame(number,FVP->GetRNValue())) result = true;
        if(is.IsSame(found,FVP->GetRNValue())) result = true;

        return(result);
}

//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// find ou the three number passed is the dv
bool SERIALSELECT::Is_VP(int number,int found)
{
        bool result = false;

        ALPHABET is;

        if(is.IsSame(number,FVP->GetRNValue())) result = true;
        if(is.IsSame(found,FVP->GetRNValue())) result = true;

        return(result);
}

//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// find ou the three number passed is the dv
bool SERIALSELECT::Is_VP(int number)
{
        bool result = false;

        ALPHABET is;

        if(is.IsSame(number,FVP->GetRNValue())) result = true;

        return(result);
}
