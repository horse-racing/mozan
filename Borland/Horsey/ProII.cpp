//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "HDV.h"
#include "HDVLV.h"
#include "HVP.h"
#include "HNEnter.h"
#include "Alpha.h"
#include "Serialsel.h"
#include "UnitForce.h"
#include "ProII.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)




//**********************************************************
// Get Number test if Ruling Figure return letter representation
//**********************************************************
AnsiString PROCESSII::GetRFigure(int value)
{
        ALPHABET is;
        AnsiString letter;

        if(is.IsSame(value,FDayValue->GetDaysValue())) letter = "A";
        if(is.IsSame(value,FHorseNames->GetFNHValue())) letter = letter + "B";
        if(is.IsSame(value,FHorseNames->GetIDValue())) letter = letter + "a";
        if(is.IsSame(value,FDvlV->GetDVLV())) letter = letter + "b";
        if(is.IsSame(value,FVP->GetRNValue())) letter = letter + "c";
        if(letter.IsEmpty()) letter = IntToStr(value);
        return(letter);
}

//---------------------------------------------------------
// Get Third part Of process 1
//
int PROCESSII::GetSecPair2(int value, int number)
{
        int Points;
        ALPHABET alpha;

        Points = alpha.GetNPointsII(value,number);

        return(alpha.GetWDO(Points,number));
}


//---------------------------------------------------------
// Get second pair 1
//
int PROCESSII::GetSecPair1(int value, int number)
{
        int Points;
        ALPHABET alpha;

        Points = alpha.GetNPointsII(value,number);

        return(alpha.GetCLO(Points,number));
}



int PROCESSII::GetFirstPair(int value, int number)
{
        int Points;
        ALPHABET alpha;

        Points = alpha.GetNPointsII(value,number);

        return(alpha.GetCLO(Points,value));
}


//----------------------------------------------------------------------------
// build an ansi string of the first line of process 1
AnsiString PROCESSII::AnsiProcess(int number, int value)
{
        AnsiString process;
        AnsiString aprocess;
        AnsiString numofrf;
        ALPHABET alpha;
        SERIALSELECT ss;

        process.printf("%d/%d-%d (%d) %d/%d-%d     ",value,number,GetFirstPair(value,number),alpha.GetNPointsII(value,number),
                number,GetSecPair1(value,number),GetSecPair2(value,number));
        aprocess.printf("RF--> %s/%s-%s (%s) %s/%s-%s    ",GetRFigure(value),GetRFigure(number),GetRFigure(GetFirstPair(value,number)),
                GetRFigure(alpha.GetNPointsII(value,number)),GetRFigure(number),GetRFigure(GetSecPair1(value,number)),GetRFigure(GetSecPair2(value,number)));
        process = process + aprocess + ss.SerialSel(value,number,GetFirstPair(value,number));
        numofrf.printf("    Number of R.F. = %d",HowManyRF(number,value));
        process = process + numofrf;
        return(process);
}


//----------------------------------------------------------------------------
// build an ansi string of the first line of process 1
AnsiString PROCESSII::AnsiProcess(int number, int value, bool uf)
{
        AnsiString process;
        AnsiString aprocess;
        AnsiString numofrf;
        ALPHABET alpha;
        SERIALSELECT ss;

        process.printf("%d/%d-%d (%d) %d/%d-%d     ",value,number,GetFirstPair(value,number),alpha.GetNPointsII(value,number),
                number,GetSecPair1(value,number),GetSecPair2(value,number));
        aprocess.printf("RF--> %s/%s-%s (%s) %s/%s-%s    ",GetRFigure(value),GetRFigure(number),GetRFigure(GetFirstPair(value,number)),
                GetRFigure(alpha.GetNPointsII(value,number)),GetRFigure(number),GetRFigure(GetSecPair1(value,number)),GetRFigure(GetSecPair2(value,number)));
        process = process + aprocess + ss.SerialSel(value,number,GetFirstPair(value,number),uf);
        numofrf.printf("    Number of R.F. = %d",HowManyRF(number,value));
        process = process + numofrf;
        return(process);
}


//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>
// find out howmany ruling figures in this process
int PROCESSII::HowManyRF(int number,int value)
{
        SERIALSELECT ss;
        ALPHABET alpha;
        int count = 0;

        count = count + ss.CountRF(value);
        count = count + ss.CountRF(number);
        count = count + ss.CountRF(GetFirstPair(value,number));
        count = count + ss.CountRF(alpha.GetNPointsII(value,number));
        count = count + ss.CountRF(GetSecPair1(value,number));
        count = count + ss.CountRF(GetSecPair2(value,number));
        return(count);
}

//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//fine the preference order
AnsiString PROCESSII::FindPreference(int number,int value)
{
        SERIALSELECT ss;
        ALPHABET alpha;
        UNITFORCE uf;
        AnsiString result;
        bool found = false;

        if(IsUOWDV(number,value))
        {
                result = "U.O.W. at the same number of points as D.V.";
                found = true;
        }
        else if(IsUOWVH(number,value))
        {
                result = "U.O.W. at the same number of points as V.H.";
                found = true;
        }
        
        if(!found)
        {
                if(IsOWDV(number,value))
                {
                        result = "O.W. at the same number of points as D.V.";
                        found = true;
                }
                else if(IsOWVH(number,value))
                {
                        result = "O.W. at the same number of points as V.H.";
                        found = true;
                }
        }

        if(!found)
        {
                if(IsUF(number,value))
                {
                        result = "Unit Force";
                        found = true;
                }
        }

        if(!found)
        {
                if(IsDF(number,value))
                {
                        result = "Double Force";
                        found = true;
                }
        }

        if(!found)
        {
        	if(IsSFF(number,value))
                {
                	result = "Special Full Force";
                        found = true;
                }
                else if(IsOrdFF(number,value))
                {
                	result = "Ordinary Full Force";
                        found = true;
                }
                else if(IsFF(number,value))
                {
                        result = "Full Force";
                        found = true;
                }

        }

        return(result);
}


//PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
// return the if this horse is UOW
bool PROCESSII::IsUOW(int number, int value)
{
        bool retval = false;

        if(IsUOWDV(number,value)) retval = true;
        if(IsUOWVH(number,value)) retval = true;

        return(retval);
}

//PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
// return the if this horse is UOW
bool PROCESSII::IsOW(int number, int value)
{
        bool retval = false;

        if(IsOWDV(number,value)) retval = true;
        if(IsOWVH(number,value)) retval = true;

        return(retval);
}

//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// test if we have a Unit Force Outright Winner with day value
bool PROCESSII::IsUOWDV(int number, int value)
{
        SERIALSELECT ss;
        ALPHABET alpha;
        UNITFORCE uf;
        bool result = false;

        if(alpha.IsSame(number,value))
        {
                number = uf.ModifyUForce(number,value);
                if(ss.Is_DV(value,number,GetFirstPair(value,number)))
                {
                        if(ss.Is_FNH(value,number,GetFirstPair(value,number)))
                        {
                                if(ss.Is_ID(value,number,GetFirstPair(value,number)))
                                {
                                        if(alpha.IsSame(FDayValue->GetDaysValue(),alpha.GetNPointsII(value,number)))
                                        {
                                                result = true;
                                        }
                                }
                                if(ss.Is_VP(value,number,GetFirstPair(value,number)))
                                {
                                        if(alpha.IsSame(FDayValue->GetDaysValue(),alpha.GetNPointsII(value,number)))
                                        {
                                                result = true;
                                        }
                                }
                        }
                }
        }
        return(result);
}


//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// test if we have a Unit Force Outright Winner with value of horse
bool PROCESSII::IsUOWVH(int number, int value)
{
        SERIALSELECT ss;
        ALPHABET alpha;
        UNITFORCE uf;
        bool result = false;

        if(alpha.IsSame(number,value))
        {
                number = uf.ModifyUForce(number,value);
                if(ss.Is_DV(value,number,GetFirstPair(value,number)))
                {
                        if(ss.Is_FNH(value,number,GetFirstPair(value,number)))
                        {
                                if(ss.Is_ID(value,number,GetFirstPair(value,number)))
                                {
                                        if(alpha.IsSame(value,alpha.GetNPointsII(value,number)))
                                        {
                                                result = true;
                                        }
                                }
                                if(ss.Is_VP(value,number,GetFirstPair(value,number)))
                                {
                                        if(alpha.IsSame(value,alpha.GetNPointsII(value,number)))
                                        {
                                                result = true;
                                        }
                                }
                        }
                }
        }
        return(result);
}



//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// test if we have a Outright Winner with day value
bool PROCESSII::IsOWDV(int number, int value)
{
        SERIALSELECT ss;
        ALPHABET alpha;
        UNITFORCE uf;
        
        bool result = false;

        if(!alpha.IsSame(number,value))
        {
                if(ss.Is_DV(value,number,GetFirstPair(value,number)))
                {
                        if(ss.Is_FNH(value,number,GetFirstPair(value,number)))
                        {
                                if(ss.Is_ID(value,number,GetFirstPair(value,number)))
                                {
                                        if(alpha.IsSame(FDayValue->GetDaysValue(),alpha.GetNPointsII(value,number)))
                                        {
                                                result = true;
                                        }
                                }
                                if(ss.Is_VP(value,number,GetFirstPair(value,number)))
                                {
                                        if(alpha.IsSame(FDayValue->GetDaysValue(),alpha.GetNPointsII(value,number)))
                                        {
                                                result = true;
                                        }
                                }
                        }
                }
        }
        return(result);
}

//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// test if we have a Outright Winner with value of horse
bool PROCESSII::IsOWVH(int number, int value)
{
        SERIALSELECT ss;
        ALPHABET alpha;
        UNITFORCE uf;
        bool result = false;

        if(!alpha.IsSame(number,value))
        {
                if(ss.Is_DV(value,number,GetFirstPair(value,number)))
                {
                        if(ss.Is_FNH(value,number,GetFirstPair(value,number)))
                        {
                                if(ss.Is_ID(value,number,GetFirstPair(value,number)))
                                {
                                        if(alpha.IsSame(value,alpha.GetNPointsII(value,number)))
                                        {
                                                result = true;
                                        }
                                }
                                if(ss.Is_VP(value,number,GetFirstPair(value,number)))
                                {
                                        if(alpha.IsSame(value,alpha.GetNPointsII(value,number)))
                                        {
                                                result = true;
                                        }
                                }
                        }
                }
        }
        return(result);
}



//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// is this a real unit force
bool PROCESSII::IsUF(int number,int value)
{
        ALPHABET alpha;
        SERIALSELECT ss;
        UNITFORCE uf;
        bool result = false;

        if(alpha.IsSame(number,value))
        {
                number = uf.ModifyUForce(number,value);
                if(ss.Is_DV(number,GetFirstPair(value,number)))
                {
                        if(ss.Is_FNH(number,GetFirstPair(value,number)))
                        {
                                if(ss.Is_ID(GetSecPair1(value,number),GetSecPair2(value,number)))
                                {
                                        result = true;
                                }
                                if(ss.Is_DVL(GetSecPair1(value,number),GetSecPair2(value,number)))
                                {
                                        result = true;
                                }
                                if(ss.Is_VP(GetSecPair1(value,number),GetSecPair2(value,number)))
                                {
                                        result = true;
                                }
                        }
                }
                if(!result)
                {
                        if(ss.Is_DV(number,GetFirstPair(value,number)))
                        {
                                if(ss.Is_FNH(number,GetFirstPair(value,number)))
                                {
                                        if(ss.Is_ID(GetSecPair1(value,number),GetSecPair2(value,number)))
                                        {
                                                if(ss.Is_VP(GetSecPair1(value,number),GetSecPair2(value,number)))
                                                {
                                                        result = true;
                                                }
                                                else if(ss.Is_DVL(GetSecPair1(value,number),GetSecPair2(value,number)))
                                                {
                                                        result = true;
                                                }
                                        }
                                }
                                else if(ss.Is_ID(number,GetFirstPair(value,number)))
                                {
                                        if(ss.Is_FNH(GetSecPair1(value,number),GetSecPair2(value,number)))
                                        {
                                                if(ss.Is_VP(GetSecPair1(value,number),GetSecPair2(value,number)))
                                                {
                                                        result = true;
                                                }
                                                else if(ss.Is_DVL(GetSecPair1(value,number),GetSecPair2(value,number)))
                                                {
                                                        result = true;
                                                }
                                        }
                                }
                                else if(ss.Is_DVL(number,GetFirstPair(value,number)))
                                {
                                        if(ss.Is_FNH(GetSecPair1(value,number),GetSecPair2(value,number)))
                                        {
                                                if(ss.Is_VP(GetSecPair1(value,number),GetSecPair2(value,number)))
                                                {
                                                        result = true;
                                                }
                                                else if(ss.Is_ID(GetSecPair1(value,number),GetSecPair2(value,number)))
                                                {
                                                        result = true;
                                                }
                                        }
                                }
                                else if(ss.Is_VP(number,GetFirstPair(value,number)))
                                {
                                        if(ss.Is_FNH(GetSecPair1(value,number),GetSecPair2(value,number)))
                                        {
                                                if(ss.Is_DVL(GetSecPair1(value,number),GetSecPair2(value,number)))
                                                {
                                                        result = true;
                                                }
                                                else if(ss.Is_ID(GetSecPair1(value,number),GetSecPair2(value,number)))
                                                {
                                                        result = true;
                                                }
                                        }
                                }
                        }
                }
        }
        return(result);
}

//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// is this a double force
bool PROCESSII::IsDF(int number,int value)
{
        ALPHABET alpha;
        SERIALSELECT ss;
        UNITFORCE uf;
        bool result = false;

        if(!IsUF(number,value))
        {
                number = uf.ModifyUForce(number,value);
                if(ss.Is_DV(GetFirstPair(value,number)))
                {
                        if(ss.Is_FNH(number))
                        {
                                if(ss.Is_ID(value))
                                {
                                        if(ss.Is_DVL(GetSecPair1(value,number),GetSecPair2(value,number)))
                                        {
                                                result = true;
                                        }
                                        if(ss.Is_VP(GetSecPair1(value,number),GetSecPair2(value,number)))
                                        {
                                                result = true;
                                        }
                                }
                        }
                }
                else if(ss.Is_FNH(GetFirstPair(value,number)))
                {
                        if(ss.Is_DV(number))
                        {
                                if(ss.Is_ID(GetSecPair1(value,number),GetSecPair2(value,number)))
                                {
                                        if(ss.Is_VP(GetSecPair1(value,number),GetSecPair2(value,number)))
                                        {
                                                result = true;
                                        }
                                        else if(ss.Is_DVL(GetSecPair1(value,number),GetSecPair2(value,number)))
                                        {
                                                result = true;
                                        }
                                }
                        }
                }
        }
        return(result);
}
//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// is this a Full force
bool PROCESSII::IsFF(int number,int value)
{
        ALPHABET alpha;
        SERIALSELECT ss;
        UNITFORCE uf;
        bool result = false;

        if(!IsUF(number,value))
        {
                number = uf.ModifyUForce(number,value);

                if(ss.Is_FNH(GetFirstPair(value,number),number))
                {
                        if(ss.Is_DV(GetSecPair1(value,number),GetSecPair2(value,number)))
                        {
                	        result = true;
                        }
                }
                else if(ss.Is_DV(GetFirstPair(value,number),number))
                {
                        if(ss.Is_FNH(GetSecPair1(value,number),GetSecPair2(value,number)))
                        {
                                result = true;
                        }
                }
        }
        return(result);
}


//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//Description:  Test for a Special full force
//
//Notes:        ID and/or FNH in the first pair
bool PROCESSII::IsSFF(int number,int value)
{
        ALPHABET alpha;
        SERIALSELECT ss;
        UNITFORCE uf;
        bool result = false;

        if(!IsUF(number,value))
        {
                number = uf.ModifyUForce(number,value);

                if(ss.Is_ID(number,GetFirstPair(value,number)))
                {
                	if(ss.Is_FNH(number,GetFirstPair(value,number)))
                        {
                        	if(ss.Is_DV(GetSecPair1(value,number),GetSecPair2(value,number)))
	                        {
                                                result = true;
                                }
                        }
                        else if(ss.Is_DV(GetSecPair1(value,number),GetSecPair2(value,number)))
                        {
                                result = true;
                        }
                }
        }
        return(result);
}

//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// is this a double force
bool PROCESSII::IsOrdFF(int number,int value)
{
        ALPHABET alpha;
        SERIALSELECT ss;
        UNITFORCE uf;
        bool result = false;

        if(!IsUF(number,value))
        {
                number = uf.ModifyUForce(number,value);

                if(ss.Is_ID(number,GetFirstPair(value,number)))
                {
			if(ss.Is_DV(number,GetFirstPair(value,number)))
        	        {
				if(ss.Is_FNH(GetSecPair1(value,number),GetSecPair2(value,number)))
				{
                        		result = true;
	                        }
                        }
                        else if(ss.Is_FNH(GetSecPair1(value,number),GetSecPair2(value,number)))
                        {
                                result = true;
                        }
                }
        }
        return(result);
}


//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// is this a double force
bool PROCESSII::Exceptions(int number,int value)
{
        ALPHABET alpha;
        SERIALSELECT ss;
        UNITFORCE uf;
        bool result = false;

        number = uf.ModifyUForce(number,value);
        if(ss.Is_FNH(GetFirstPair(value,number)))
        {
                if(ss.Is_ID(GetSecPair1(value,number),GetSecPair2(value,number)))
                {
                        result = true;
                }
                if(ss.Is_DVL(GetSecPair1(value,number),GetSecPair2(value,number)))
                {
                        result = true;
                }
                if(ss.Is_VP(GetSecPair1(value,number),GetSecPair2(value,number)))
                {
                        result = true;
                }
        }
        else if(ss.Is_ID(GetFirstPair(value,number)))
        {
                if(ss.Is_FNH(GetSecPair1(value,number),GetSecPair2(value,number)))
                {
                        result = true;
                }
                if(ss.Is_DVL(GetSecPair1(value,number),GetSecPair2(value,number)))
                {
                        result = true;
                }
                if(ss.Is_VP(GetSecPair1(value,number),GetSecPair2(value,number)))
                {
                        result = true;
                }
        }
        return(result);
}

