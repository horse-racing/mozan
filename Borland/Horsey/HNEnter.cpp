//---------------------------------------------------------------------------

#include <vcl.h>
#include <stdio.h>
#include <inifiles.hpp>
#include <dir.h>
#pragma hdrstop

#include "Alpha.h"
#include "UnitForce.h"
#include "ProI.h"
#include "ProII.h"
#include "HDV.h"
#include "Serialsel.h"
#include "HNEnter.h"
#include "RulingFigures.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"

#define CR 0x0d

int Line=1;

TFHorseNames *FHorseNames;
//---------------------------------------------------------------------------
__fastcall TFHorseNames::TFHorseNames(TComponent* Owner)
        : TForm(Owner)
{
        TIniFile *ini;

        ini = new TIniFile(ChangeFileExt(Application->ExeName,".INI"));
        Top = ini->ReadInteger("HorseForm","Top",10);
        Left = ini->ReadInteger("HorseForm","Left",10);
        delete ini;

}
//---------------------------------------------------------------------------
void __fastcall TFHorseNames::EhorseChange(TObject *Sender)
{
        int select =-1;
        int x;
        int stat;

        if(CheckBox1->Checked)
        {
                Ehorse->Text = Ehorse->Text.UpperCase();
                Ehorse->SelStart =  255;
        }
        Ehvalue->Text = HorseValue(Ehorse->Text);
        if(Ehorse->Text != "")
        {
                select = ListHorseHis->Items->IndexOf(Ehorse->Text);
                if(select == -1)
                {
                        for(x=0; x<ListHorseHis->Items->Count; x++)
                        {
                                stat = ListHorseHis->Items->Strings[x].AnsiPos(Ehorse->Text);
                                if(stat > 0)
                                {
                                        select = x;
                                        break;
                                }
                        }
                }
        }
        ListHorseHis->ItemIndex = select;

}
//---------------------------------------------------------------------------
void __fastcall TFHorseNames::Edit1KPress(TObject *Sender, char &Key)
{
        int x;
        int lb1flag = FALSE;
        int lb2flag = FALSE;

        if(Key == CR)
        {
                for(x=0; x<ListRHorse->Items->Count; x++)
                {
                        if(ListRHorse->Items->Strings[x].AnsiCompareIC(Ehorse->Text)== 0)
                        {
                                lb2flag = TRUE;
                        }
                }
                if(!lb2flag)
                {
                        ListRHorse->Items->Add(Ehorse->Text);
                        ListRHorse->ItemIndex= ListRHorse->Items->Count-1;
                }

                for(x=0; x<ListHorseHis->Items->Count; x++)
                {
                        if(ListHorseHis->Items->Strings[x].AnsiCompareIC(Ehorse->Text) == 0)
                        {
                                lb1flag = TRUE;
                        }
                }
                if(!lb1flag) ListHorseHis->Items->Add(Ehorse->Text);

                Ehorse->Text = "";
        }
        ListRHorseClick(Sender);
}
//---------------------------------------------------------------------------
void __fastcall TFHorseNames::CheckBox1Click(TObject *Sender)
{
        Ehorse->SetFocus();
        Ehorse->SelStart =  255;
}
//---------------------------------------------------------------------------

void __fastcall TFHorseNames::BHisToHorClick(TObject *Sender)
{
        if(ListHorseHis->ItemIndex > -1)
        {
                ListRHorse->Items->Add(ListHorseHis->Items->Strings[ListHorseHis->ItemIndex]);
                ListRHorse->ItemIndex = ListRHorse->Items->Count-1;
        }
        ListRHorseClick(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TFHorseNames::Button2Click(TObject *Sender)
{
        if(ListHorseHis->ItemIndex > -1)
        {
                ListHorseHis->Items->Delete(ListHorseHis->ItemIndex);
        }
}
//---------------------------------------------------------------------------

void __fastcall TFHorseNames::Button3Click(TObject *Sender)
{
        ListHorseHis->Items->SaveToFile("HHistory.lst");
}

//---------------------------------------------------------------------------

void __fastcall TFHorseNames::ListRHorseClick(TObject *Sender)
{
        int ListNum;

        ListNum = ListRHorse->ItemIndex;
        if(ListNum > -1)
        {
                Edit3->Text = ListRHorse->ItemIndex+1;
        }
        else Edit3->Text = '0';
}
//---------------------------------------------------------------------------



void __fastcall TFHorseNames::FormShow(TObject *Sender)
{
        int NumHorse;
        int x;
        char *Buffer;

        Buffer = searchpath("HHistory.lst");
        if(Buffer)
        {
                ListHorseHis->Items->LoadFromFile(Buffer);
        }

}
//---------------------------------------------------------------------------


void __fastcall TFHorseNames::Button5Click(TObject *Sender)
{
        int ListNum;

        ListNum = ListRHorse->ItemIndex;
        if(ListNum > -1)
        {
                Ehorse->Text = ListRHorse->Items->Strings[ListNum];
                ListRHorse->Items->Delete(ListNum);
                Ehorse->SetFocus();
                Ehorse->SelectAll();
        }
}
//---------------------------------------------------------------------------




void __fastcall TFHorseNames::FormClose(TObject *Sender,
      TCloseAction &Action)
{
        TIniFile *ini;
        AnsiString Buffer;

        Buffer = searchpath("HHistory.lst");
        if(!Buffer.IsEmpty())
        {
                ListHorseHis->Items->SaveToFile(Buffer);
        }


        ini = new TIniFile(ChangeFileExt(Application->ExeName,".INI"));
        ini->WriteInteger("HorseForm","Top",Top);
        ini->WriteInteger("HorseForm","Left",Left);

        delete ini;


}
//---------------------------------------------------------------------------

void __fastcall TFHorseNames::BMupClick(TObject *Sender)
{
        int itemindex;

        if(ListRHorse->Items->Count)
        {
                itemindex = ListRHorse->ItemIndex;
                if(itemindex != 0)
                {
                        ListRHorse->Items->Move(itemindex,itemindex-1);
                        ListRHorse->ItemIndex = itemindex-1;
                }
                ListRHorseClick(Sender);
        }
}
//---------------------------------------------------------------------------

void __fastcall TFHorseNames::BMdwnClick(TObject *Sender)
{
        int itemindex;

        if(ListRHorse->Items->Count)
        {
                itemindex = ListRHorse->ItemIndex;
                if(itemindex != ListRHorse->Items->Count-1)
                {
                        ListRHorse->Items->Move(itemindex,itemindex+1);
                        ListRHorse->ItemIndex = itemindex+1;
                }
                ListRHorseClick(Sender);
        }
}
//---------------------------------------------------------------------------
// Get the full number of the horse value
int TFHorseNames::GetFNHValue(void)
{
        int number = 0;
        RF rf;

        number = ListRHorse->Items->Count;

        if(number < 0) number = 0;
        return(rf.FullNumHorse(number));
}

//----------------------------------------------------------------------------
// Get the Initials' Digit (I.D.)
int TFHorseNames::GetIDValue(void)
{
        int x,NumAdd;
        AnsiString Horse;
        char ch;
        ALPHABET util;
        SINGLE srink;

        NumAdd=0;
        for(x=0; x<ListRHorse->Items->Count; x++)
        {
                Horse = ListRHorse->Items->Strings[x];
                ch = util.Get_Char(Horse,1);
                NumAdd += GetInitalDigit(ch);
                NumAdd = srink.single(NumAdd);
        }
        return NumAdd;
}

//--------------------------------------------------------
// Get a progressive Name Value
//-------------------------------------------------------
int TFHorseNames::HorseValue(AnsiString AString)
{
        int NameLen, Value, x, NumAdd;
        char first;
        AnsiString Temp;
        ALPHABET abet;
        SINGLE singles;

        NameLen = AString.Length();
        NumAdd = abet.Get_First_Value(AString,1);

        for(x=2;x<=NameLen;x++)
        {
                first = abet.Get_Char(AString,x);
                Value = abet.Get_Egypt_Value(first);
                if(!Value)
                {
                        x++;
                        NumAdd += abet.Get_First_Value(AString,x);

                }
                NumAdd += Value;
                NumAdd = singles.single(NumAdd);
        }

        return NumAdd;
}


//--------------------------------------------------------
// Get letter Value
//
int TFHorseNames::GetInitalDigit(char Letter)
{
        switch (Letter)
        {
                case 'a': case 'A':
                case 'i': case 'I':
                case 'y': case 'Y':
                        return 1;
                case 'b': case 'B':
                case 'k': case 'K':
                case 'r': case 'R':
                        return 2;
                case 'c': case 'C':
                case 'g': case 'G':
                case 'j': case 'J':
                case 'l': case 'L':
                case 'q': case 'Q':
                        return 3;
                case 'd': case 'D':
                case 'm': case 'M':
                case 't': case 'T':
                        return 4;
                case 'e': case 'E':
                case 'n': case 'N':
                        return 5;
                case 's': case 'S':
                case 'u': case 'U':
                case 'v': case 'V':
                case 'w': case 'W':
                        return 6;
                case 'x': case 'X':
                case 'z': case 'Z':
                        return 7;
                case 'f': case 'F':
                case 'h': case 'H':
                case 'o': case 'O':
                case 'p': case 'P':
                        return 8;
                default:
                        return 0;
        }
}

void __fastcall TFHorseNames::BListDelClick(TObject *Sender)
{
        if(ListRHorse->Items->Count)
        {
                ListRHorse->Items->Delete(ListRHorse->ItemIndex);
                ListRHorseClick(Sender);
        }
}
//---------------------------------------------------------------------------
// return the number of horses on the race
int TFHorseNames::Count(void)
{
        return(ListRHorse->Items->Count);
}

//--------------------------------------------------------------------------
// return the name of the horse number
AnsiString TFHorseNames::HorseName(int number)
{
        AnsiString ahorse;

        if(number < ListRHorse->Items->Count)
        {
                ahorse = ListRHorse->Items->Strings[number];
        }

        return(ahorse);
}

//---------------------------------------------------------------------------
// return the horse value buty horse number
int TFHorseNames::HorseValue(int number)
{
        AnsiString ahorse;
        int value = 0;


        if(number < ListRHorse->Items->Count)
        {
                ahorse = ListRHorse->Items->Strings[number];
                value = HorseValue(ahorse);
        }
        return(value);
}


//---------------------------------------------------------------------------
// Return First pair of Process I
AnsiString TFHorseNames::ProIFPair(int number)
{
        bool ufflag=false;
        int value = 0;
        AnsiString process;
        UNITFORCE uforce;
        PROCESSI proi;
        SINGLE sn;
        ALPHABET alf;

        if(number < ListRHorse->Items->Count)
        {
                value = HorseValue(number);
                number = sn.single(number+1);
                if(alf.IsSame(number,value)) ufflag = true;
                number = uforce.ModifyUForce(number,value);
                process = proi.AnsiProcess(number,value,ufflag);
        }
        return(process);
}


AnsiString TFHorseNames::ProIIFPair(int number)
{
        bool ufflag = false;
        int value = 0;
        AnsiString process;
        UNITFORCE uforce;
        PROCESSII proii;
        SINGLE sn;
        ALPHABET alf;

        if(number < ListRHorse->Items->Count)
        {
                value = HorseValue(number);
                number = sn.single(number+1);
                if(alf.IsSame(number,value)) ufflag = true;
                number = uforce.ModifyUForce(number,value);
                process = proii.AnsiProcess(number,value,ufflag);
        }
        return(process);

}

void __fastcall TFHorseNames::BTransferClick(TObject *Sender)
{
        TIniFile *ini;

        ini = new TIniFile(ChangeFileExt(Application->ExeName,".INI"));
        ini->WriteInteger("HorseForm","Top",Top);
        ini->WriteInteger("HorseForm","Left",Left);

        FHorseNames->Hide();
        delete ini;
}






//---------------------------------------------------------------------------
// find the Direct connection
AnsiString TFHorseNames::FindDC(int number)
{
        int value = 0;
        int bits = 0;
        AnsiString direct;
        SINGLE sn;
        SERIALSELECT ss;


        if(number < ListRHorse->Items->Count)
        {
                value = HorseValue(number);
                number = sn.single(number+1);
                bits = ss.Is_DC(number,value);
                switch(bits)
                {
                        case 1:
                                direct = "in value of the horse";
                                break;
                        case 2:
                                direct = "in number of the horse";
                                break;
                        case 3:
                                direct ="in both Value and Number of the horse";
                                break;
                        case 0:
                        default:
                                direct = "";
                                break;
                }
        }
        return(direct);


}



//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// enter horse name externally
void TFHorseNames::AddHorse(char *horsename)
{
        ListRHorse->Items->Add(horsename);
}



//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// Total number of RF's in the processes
int TFHorseNames::TotalRF(int number)
{
        int value = 0;
        int total = 0;
        UNITFORCE uforce;
        PROCESSI proi;
        PROCESSII proii;
        SINGLE sn;


        if(number < ListRHorse->Items->Count)
        {
                value = HorseValue(number);
                number = sn.single(number+1);
                number = uforce.ModifyUForce(number,value);
                total = total + proi.HowManyRF(number,value);
                total = total + proii.HowManyRF(number,value);
        }
        return(total);


}

//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// get the string of the preference order
AnsiString TFHorseNames::PrefProI(int number)
{
        AnsiString result;
        int value = 0;
        PROCESSI proi;
        SINGLE sn;
        SERIALSELECT ss;

        if(number < ListRHorse->Items->Count)
        {
                value = HorseValue(number);
                number = sn.single(number+1);

                if(ss.Is_DC(number,value) != 0)
                {
                        result = proi.FindPreference(number,value);
                }
        }

        return(result);
}


//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
// return if this horse is UOW
bool TFHorseNames::Pro1IsUOW(int number)
{
        bool retval = false;
        int value = 0;
        SINGLE sn;
        PROCESSI proi;
        SERIALSELECT ss;

        if(number < ListRHorse->Items->Count)
        {
                value = HorseValue(number);
                number = sn.single(number+1);

                if(ss.Is_DC(number,value))
                {
                        if(proi.IsUOW(number,value)) retval = true;
                }
        }
        return(retval);
}


//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
// return if this horse is OW
bool TFHorseNames::Pro1IsOW(int number)
{
        bool retval = false;
        int value = 0;
        SINGLE sn;
        PROCESSI proi;
        SERIALSELECT ss;

        if(number < ListRHorse->Items->Count)
        {
                value = HorseValue(number);
                number = sn.single(number+1);
                if(ss.Is_DC(number,value))
                {
                        if(proi.IsOW(number,value)) retval = true;
                }
        }
        return(retval);
}

//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
// return if this horse is OW
bool TFHorseNames::Pro1IsUF(int number)
{
        bool retval = false;
        int value = 0;
        SINGLE sn;
        PROCESSI proi;
        SERIALSELECT ss;

        if(number < ListRHorse->Items->Count)
        {
                value = HorseValue(number);
                number = sn.single(number+1);

                if(ss.Is_DC(number,value))
                {
                        if(proi.IsUF(number,value)) retval = true;
                }
        }
        return(retval);
}

//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
// return if this horse is OW
bool TFHorseNames::Pro1IsDF(int number)
{
        bool retval = false;
        int value = 0;
        SINGLE sn;
        PROCESSI proi;
        SERIALSELECT ss;

        if(number < ListRHorse->Items->Count)
        {
                value = HorseValue(number);
                number = sn.single(number+1);

                if(ss.Is_DC(number,value))
                {
                        if(proi.IsDF(number,value)) retval = true;
                }
        }
        return(retval);
}

//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
// return if this horse is OW
bool TFHorseNames::Pro1IsFF(int number)
{
        bool retval = false;
        int value = 0;
        SINGLE sn;
        PROCESSI proi;
        SERIALSELECT ss;

        if(number < ListRHorse->Items->Count)
        {
                value = HorseValue(number);
                number = sn.single(number+1);

                if(ss.Is_DC(number,value))
                {
                        if(proi.IsFF(number,value)) retval = true;
                        if(proi.IsOrdFF(number,value)) retval = true;
                        if(proi.IsSFF(number,value)) retval = true;
                }
        }
        return(retval);
}


//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// get the string of the preference order
AnsiString TFHorseNames::PrefProII(int number)
{
        AnsiString result;
        int value = 0;
        PROCESSII proii;
        SINGLE sn;
        SERIALSELECT ss;

        if(number < ListRHorse->Items->Count)
        {
                value = HorseValue(number);
                number = sn.single(number+1);
                if(ss.Is_DC(number,value))
                {
                        result = proii.FindPreference(number,value);
                }
        }

        return(result);
}

//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
// return true if this horse is UOW
bool TFHorseNames::Pro2IsUOW(int number)
{
        bool retval = false;
        int value = 0;
        SINGLE sn;
        PROCESSII proii;
        SERIALSELECT ss;

        if(number < ListRHorse->Items->Count)
        {
                value = HorseValue(number);
                number = sn.single(number+1);

                if(ss.Is_DC(number,value))
                {
                        if(proii.IsUOW(number,value)) retval = true;
                }
        }
        return(retval);
}

//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
// return if this horse is OW
bool TFHorseNames::Pro2IsOW(int number)
{
        bool retval = false;
        int value = 0;
        SINGLE sn;
        PROCESSII proii;
        SERIALSELECT ss;

        if(number < ListRHorse->Items->Count)
        {
                value = HorseValue(number);
                number = sn.single(number+1);

                if(ss.Is_DC(number,value))
                {
                        if(proii.IsOW(number,value)) retval = true;
                }
        }
        return(retval);
}

//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
// return if this horse is OW
bool TFHorseNames::Pro2IsUF(int number)
{
        bool retval = false;
        int value = 0;
        SINGLE sn;
        PROCESSII proii;
        SERIALSELECT ss;

        if(number < ListRHorse->Items->Count)
        {
                value = HorseValue(number);
                number = sn.single(number+1);

                if(ss.Is_DC(number,value))
                {
                        if(proii.IsUF(number,value)) retval = true;
                }
        }
        return(retval);
}

//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
// return if this horse is OW
bool TFHorseNames::Pro2IsDF(int number)
{
        bool retval = false;
        int value = 0;
        SINGLE sn;
        PROCESSII proii;
        SERIALSELECT ss;

        if(number < ListRHorse->Items->Count)
        {
                value = HorseValue(number);
                number = sn.single(number+1);

                if(ss.Is_DC(number,value))
                {
                        if(proii.IsDF(number,value)) retval = true;
                }
        }
        return(retval);
}

//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
// return if this horse is OW
bool TFHorseNames::Pro2IsFF(int number)
{
        bool retval = false;
        int value = 0;
        SINGLE sn;
        PROCESSII proii;
        SERIALSELECT ss;

        if(number < ListRHorse->Items->Count)
        {
                value = HorseValue(number);
                number = sn.single(number+1);

                if(ss.Is_DC(number,value))
                {
                        if(proii.IsFF(number,value)) retval = true;
                        if(proii.IsOrdFF(number,value)) retval = true;
                        if(proii.IsSFF(number,value)) retval = true;
                }
        }
        return(retval);
}

//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// clear the all the horses in the horse list
void TFHorseNames::ClearHorse(void)
{
        ListRHorse->Clear();
}

//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// find the exception
AnsiString TFHorseNames::Exceptions(int number)
{
        AnsiString result;
        int value = 0;
        PROCESSI proi;
        PROCESSII proii;
        SINGLE sn;
        SERIALSELECT ss;

        if(number < ListRHorse->Items->Count)
        {
                value = HorseValue(number);
                number = sn.single(number+1);

                if(ss.Is_DC(number,value))
                {
                        if(proi.Exceptions(number,value))
                        {
                                if(proii.Exceptions(number,value))
                                {
                                        result = "Exception Found";
                                }
                        }
                }
        }

        return(result);


}

//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// find the exception
bool TFHorseNames::IsException(int number)
{
        bool retval = false;
        int value = 0;
        PROCESSI proi;
        PROCESSII proii;
        SINGLE sn;
        SERIALSELECT ss;

        if(number < ListRHorse->Items->Count)
        {
                value = HorseValue(number);
                number = sn.single(number+1);

                if(ss.Is_DC(number,value))
                {
                        if(proi.Exceptions(number,value))
                        {
                                if(proii.Exceptions(number,value))
                                {
                                        retval = true;
                                }
                        }
                }
        }
        return(retval);
}

//*************************************
void TFHorseNames::ExHName(char *s)
{
        char *RetPtr;

        while(*s != ',') s++;
        s++;
        while(*s != ',') s++;
        s++;
        while(*s != ',') s++;
        s++;
        RetPtr=s;
        while((*s != ',') && (*s != '(')) s++;

        *s=0;
        AddHorse(RetPtr);
}

void __fastcall TFHorseNames::Button1Click(TObject *Sender)
{
        ListRHorse->Clear();
}
//---------------------------------------------------------------------------


