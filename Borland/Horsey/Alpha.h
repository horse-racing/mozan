//---------------------------------------------------------------------------

#ifndef AlphaH
#define AlphaH
//---------------------------------------------------------------------------
class ALPHABET
{
        private:


        public:
        char Get_Char(AnsiString Name, int Start);
        int Get_Egypt_Value(char Letter);
        int Get_First_Value(AnsiString str, int index);
        int Get_Hebrew_Value(char Letter);
        bool IsVowel(char Letter);
        bool IsSame(int number,int value);
        int GetNPointsI(int value, int number);
        int GetNPointsII(int value, int number);
        int GetWDO(int points, int number);
        int GetCLO(int points, int number);
};
#endif
